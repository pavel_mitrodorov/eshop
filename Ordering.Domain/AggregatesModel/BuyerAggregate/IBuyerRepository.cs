﻿namespace Ordering.Domain.AggregatesModel.BuyerAggregate;

public interface IBuyerRepository : IRepository<Buyer>
{
    Task AddAsync(Buyer buyer, CancellationToken cancellationToken = default);
    Buyer Update(Buyer buyer);
    Task<Buyer> FindByUserIdentityAsync(string userIdentity, CancellationToken cancellationToken = default);
    Task<Buyer> FindByIdAsync(int id, CancellationToken cancellationToken = default);
}
