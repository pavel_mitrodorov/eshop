﻿namespace Ordering.Domain.AggregatesModel.OrderAggregate;

public class Address
    :ValueObject
{
    public String Street { get; init; }
    public String City { get; init; }
    public String State { get; init; }
    public String Country { get; init; }
    public String ZipCode { get; init; }

    public Address() { }

    public Address(string street, string city, string state, string country, string zipcode)
    {
        Street = street;
        City = city;
        State = state;
        Country = country;
        ZipCode = zipcode;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Street;
        yield return City;
        yield return State;
        yield return Country;
        yield return ZipCode;
    }
}
