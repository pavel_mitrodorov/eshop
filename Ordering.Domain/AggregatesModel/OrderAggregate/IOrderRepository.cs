﻿namespace Ordering.Domain.AggregatesModel.OrderAggregate;

public interface IOrderRepository : IRepository<Order>
{
    Task AddAsync(Order order, CancellationToken cancellationToken = default);

    void Update(Order order);

    Task<Order> GetAsync(int orderId, CancellationToken cancellationToken = default);
}
