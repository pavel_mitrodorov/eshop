﻿global using MediatR;
global using Ordering.Domain.Common;
global using System.Reflection;
global using Ordering.Domain.Exceptions;
global using Ordering.Domain.AggregatesModel.BuyerAggregate;
global using Ordering.Domain.Events;
global using Ordering.Domain.AggregatesModel.OrderAggregate;