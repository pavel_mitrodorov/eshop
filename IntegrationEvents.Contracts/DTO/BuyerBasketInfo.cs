﻿namespace IntegrationEvents.Contracts.DTO;

public record BuyerBasketInfo
{
    public string City { get; init; }

    public string Street { get; init; }

    public string State { get; init; }

    public string Country { get; init; }

    public string ZipCode { get; init; }

    public string CardBindingId { get; init; }
}
