﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationEvents.Contracts.DTO
{
    public record ConfirmedOrderStockItem
    {
        public int ProductId { get; init;}
        public bool HasStock { get; init; }
    }
}
