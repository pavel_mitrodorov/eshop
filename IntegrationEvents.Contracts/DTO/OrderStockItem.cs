﻿namespace IntegrationEvents.Contracts.DTO;
public record OrderStockItem
{
    public int ProductId { get; }
    public int Units { get; }

}
