﻿namespace IntegrationEvents.Contracts.Event;

public interface GracePeriodConfirmedIntegrationEvent
{
    public int OrderId { get; }
}
