﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToShippedIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
}
