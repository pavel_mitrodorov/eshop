﻿namespace IntegrationEvents.Contracts.Event;
public interface ProductPriceChangedIntegrationEvent
{
    public int ProductId { get; }

    public decimal NewPrice { get; }

    public decimal OldPrice { get; }
}