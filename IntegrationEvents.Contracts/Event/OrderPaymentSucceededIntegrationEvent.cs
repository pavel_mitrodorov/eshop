﻿namespace IntegrationEvents.Contracts.Event;
public interface OrderPaymentSucceededIntegrationEvent
{
    public int OrderId { get; }
}
