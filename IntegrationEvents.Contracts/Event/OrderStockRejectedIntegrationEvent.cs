﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStockRejectedIntegrationEvent
{
    public int OrderId { get; }

    public List<ConfirmedOrderStockItem> OrderStockItems { get; }
}
