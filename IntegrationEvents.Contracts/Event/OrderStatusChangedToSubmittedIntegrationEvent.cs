﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToSubmittedIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
}
