﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToStockConfirmedIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
}
