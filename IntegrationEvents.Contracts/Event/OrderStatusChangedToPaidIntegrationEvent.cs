﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToPaidIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
    public IEnumerable<OrderStockItem> OrderStockItems { get; }

}