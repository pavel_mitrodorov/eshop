﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToCancelledIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
}
