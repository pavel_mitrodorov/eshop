﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderPaymentFailedIntegrationEvent
{
    public int OrderId { get; }
}
