﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStartedIntegrationEvent
{
    public string UserId { get; }
}
