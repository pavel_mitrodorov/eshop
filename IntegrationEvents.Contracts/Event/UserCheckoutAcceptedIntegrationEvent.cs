﻿namespace IntegrationEvents.Contracts.Event;

public interface UserCheckoutAcceptedIntegrationEvent
{
    public Buyer Buyer { get; }

    public Guid RequestId { get; }

    public IEnumerable<BasketItem> Items { get; }

    public BuyerBasketInfo  BuyerBasketInfo { get; }
}
