﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStockConfirmedIntegrationEvent
{
    public int OrderId { get; }
}
