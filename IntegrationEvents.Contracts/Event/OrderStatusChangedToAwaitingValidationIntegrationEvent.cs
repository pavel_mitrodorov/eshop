﻿namespace IntegrationEvents.Contracts.Event;

public interface OrderStatusChangedToAwaitingValidationIntegrationEvent
{
    public int OrderId { get; }
    public string OrderStatus { get; }
    public string BuyerName { get; }
    public IEnumerable<OrderStockItem> OrderStockItems { get; }
}
