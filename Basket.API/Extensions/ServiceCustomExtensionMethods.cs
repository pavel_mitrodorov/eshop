﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;

namespace Basket.API.Extensions;

public static class ServiceCustomExtensionMethods
{

    public static IServiceCollection AddCustomServices(this IServiceCollection services)
    {
        services
            .AddTransient<IIdentityService, IdentityService>();
        return services;
    }

    public static IServiceCollection AddAutoMappper(this IServiceCollection services)
    {
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        services.AddSingleton(mapperConfig.CreateMapper());
        return services;
    }
    public static void ConfigureAuthService(IServiceCollection services, IConfiguration configuration)
    {
        // prevent from mapping "sub" claim to nameidentifier.
        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");

        var identityUrl = configuration["IdentityUrl"];

        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

        }).AddJwtBearer(options =>
        {
            options.Authority = identityUrl;
            options.RequireHttpsMetadata = false;
            options.Audience = "basket";
        });
    }
    public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddMassTransit(x =>
            {
                x.AddConsumer<OrderStartedIntegrationEventConsumer>();
                x.AddConsumer<ProductPriceChangedIntegrationEventConsumer>();
                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["RabbitMQ:host"], configurator =>
                    {
                        configurator.Username(configuration["RabbitMQ:user"]);
                        configurator.Password(configuration["RabbitMQ:password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                    cfg.UseMessageRetry(r => r.Immediate(3));
                });
            });
        return services;
    }
    public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
    {
        var hcBuilder = services.AddHealthChecks();
        hcBuilder
            .AddRedis(
                configuration["ConnectionString"],
                name: "redis-check",
                tags: new [] { "redis" });
        //.AddRabbitMQ() -- HealthCheck на eventbus уже включен по умолчанию в MassTransit


        return services;
    }
    public static IServiceCollection AddRedis(this IServiceCollection services, IConfiguration configuration)
    {
        var c = ConnectionMultiplexer.Connect($"{configuration["ConnectionString"]},allowAdmin=true");
        services.AddSingleton<ConnectionMultiplexer>(c);
        services.AddTransient<IBasketRepository, RedisBasketRepository>();
        return services;
    }

    public static IServiceCollection AddCustomMvc(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddControllers(options => {
                options.SuppressAsyncSuffixInActionNames = false;
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));

            })
            .ConfigureApiBehaviorOptions(op =>
                op.SuppressModelStateInvalidFilter = true
            )
            .AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);

        return services;
    }
}
