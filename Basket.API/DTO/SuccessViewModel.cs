﻿namespace Basket.API.DTO;

public class SuccessViewModel
{
    public bool Success { get; set; }
    public string Remark { get; set; }
    public SuccessViewModel()
    {
        Success = true;
    }
    public SuccessViewModel(bool success)
    {
        Success = success;
    }
    public SuccessViewModel(bool success, string remark)
        :this(success)
    {
        Remark = remark;
    }
}

