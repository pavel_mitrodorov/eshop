﻿

namespace Basket.API.DTO
{
    public class BasketCheckoutDto
    {
        [Required(ErrorMessage = "Не указано City")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string City { get; set; }

        [Required(ErrorMessage = "Не указано Street")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
        public string Street { get; set; }

        public string State { get; set; }

        [Required(ErrorMessage = "Не указано Country")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Country { get; set; }

        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Не указано CardBindingID")]
        public string CardBindingId { get; set; }

    }
}
