﻿namespace Basket.API.DTO;

public class BasketItemDto
{
    [Required(ErrorMessage = "Не указано Id")]
    public string Id { get; set; }

    [Required(ErrorMessage = "Не указано ProductId")]
    public int ProductId { get; set; }

    [Required(ErrorMessage = "Не указано ProductName")]
    [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
    public string ProductName { get; set; }

    [Required(ErrorMessage = "Не указано UnitPrice")]
    [Range(0, double.MaxValue, ErrorMessage = "Недопустимая цена")]
    public decimal UnitPrice { get; set; }

    [Required(ErrorMessage = "Не указано Quantity")]
    [Range(0, double.MaxValue, ErrorMessage = "Недопустимое кол-во")]
    public int Quantity { get; set; }

    public string PictureUrl { get; set; }
}
