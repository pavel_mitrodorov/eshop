﻿namespace Basket.API.Infrastructure.Repositories;

public interface IBasketRepository
{
    Task<CustomerBasket> GetAsync(string buyerId);
    IEnumerable<string> GetAllBuyers();
    Task<CustomerBasket> UpdateAsync(CustomerBasket basket);
    Task<bool> DeleteAsync(string buyerId);
}
