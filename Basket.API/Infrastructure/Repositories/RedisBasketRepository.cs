﻿namespace Basket.API.Infrastructure.Repositories
{
    public class RedisBasketRepository : IBasketRepository
    {
        private readonly ILogger<RedisBasketRepository> _logger;
        private readonly ConnectionMultiplexer _redis;
        private readonly IDatabase _database;

        public RedisBasketRepository(ILogger<RedisBasketRepository> logger, ConnectionMultiplexer redis)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _redis = redis ?? throw new ArgumentNullException(nameof(redis));
            _database = _redis.GetDatabase();
        }

        public async Task<bool> DeleteAsync(string buyerId)
        {
            return await _database.KeyDeleteAsync(buyerId);
        }

        public async Task<CustomerBasket> GetAsync(string buyerId)
        {
            var data = await _database.StringGetAsync(buyerId);
            if (data.IsNullOrEmpty)
            {
                return null;
            }

            return JsonSerializer.Deserialize<CustomerBasket>(data, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }

        public IEnumerable<string> GetAllBuyers()
        {
            var server = GetServer();
            var data =  server.Keys();
            return data?.Select(x => x.ToString());
        }

        public async Task<CustomerBasket> UpdateAsync(CustomerBasket basket)
        {
            var created = await _database.StringSetAsync(basket.BuyerId, JsonSerializer.Serialize(basket));
            if (!created)
            {
                throw new BasketDomainException($"Problem occur persisting the item. BuyerId: {basket.BuyerId}");
            }

            _logger.LogInformation("Basket item persisted successfully");
            return await GetAsync(basket.BuyerId);
        }

        private IServer GetServer()
        {
            var endpoint = _redis.GetEndPoints();
            return _redis.GetServer(endpoint.First());
        }
    }
}
