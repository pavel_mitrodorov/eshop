﻿namespace Basket.API.Infrastructure.Middlewares
{
    public class FailingStartupFilter
        : IStartupFilter

    {
        private readonly Action<FailingOptions> _action;
        public FailingStartupFilter(Action<FailingOptions> optionsAction)
        {
            _action = optionsAction;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            var options = new FailingOptions();
            _action.Invoke(options);
            return app =>
            {
                app.UseMiddleware<FailingMiddleware>(options);
                next(app);
            };
        }
    }
}
