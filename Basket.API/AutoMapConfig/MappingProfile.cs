﻿

namespace Basket.API.AutoMapConfig;

public class MappingProfile : Profile
{
    public MappingProfile()
    {

        CreateMap<BasketItemDto, BasketItem>();

    }
}
