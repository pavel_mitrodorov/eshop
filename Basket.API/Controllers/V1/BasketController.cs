﻿namespace Basket.API.Controllers.V1;

/// <summary>
/// Контрейлер в котором содержатся методы для работы с корзиной
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class BasketController : ControllerBase
{
    private readonly IBasketRepository _basketRepository;
    private readonly IPublishEndpoint _endpoint;
    private readonly string _userId;
    private readonly IMapper _mapper;

    public BasketController(IBasketRepository basketRepository, 
        IPublishEndpoint endpoint, 
        IIdentityService identityService,
        IMapper mapper)
    {
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
        _endpoint = endpoint ?? throw new ArgumentNullException(nameof(endpoint));
        _userId = identityService.GetUserIdentity();
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    /// <summary>получение корзины пользователя</summary>
    /// <returns>корзина</returns>
    [HttpGet()]
    [ProducesResponseType(typeof(CustomerBasket), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAsync()
    {
        var basket = await _basketRepository.GetAsync(_userId);
        return Ok(basket ?? new CustomerBasket() { BuyerId = _userId });
    }

    /// <summary>обновление корзины пользователя</summary>
    /// <param name="items">корзина</param>
    /// <returns>корзина</returns>
    [HttpPost]
    [ProducesResponseType(typeof(CustomerBasket), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateAsync([FromBody] List<BasketItemDto> items)
    {
        var updCustomerBasket = new CustomerBasket()
        {
            BuyerId = _userId,
            Items = _mapper.Map<List<BasketItem>>(items)
        };
        return Ok(await _basketRepository.UpdateAsync(updCustomerBasket));
    }

    /// <summary>удаление корзины пользователя</summary>
    /// <returns>ОК</returns>
    [HttpDelete()]
    [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> DeleteAsync()
    {
        bool success = await _basketRepository.DeleteAsync(_userId);
        if (!success)
        {
            return BadRequest(new ErrorViewModel("не удалось удалить корзину!"));
        }
        return Ok(new SuccessViewModel());    
    }
    

    /// <summary>оформление покупки корзины пользователя</summary>
    /// <param name="basketCheckout">данные о покупке</param>
    /// <returns>Accepted</returns>
    [Route("checkout")]
    [HttpPost]
    [ProducesResponseType(typeof(int), StatusCodes.Status202Accepted)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CheckoutAsync([FromBody] BasketCheckoutDto basketCheckout)
    {
        var basket = await _basketRepository.GetAsync(_userId);
        if (basket == null)
        {
            return NotFound(new ErrorViewModel("Не удалось найти корзину пользователя!"));
        }
        if(!basket.Items.Any())
        {
            return BadRequest(new ErrorViewModel("Корзина пользователя пустая!"));
        }
        await _endpoint.Publish<UserCheckoutAcceptedIntegrationEvent>(new
        {
            buyer =  new { Id = basket.BuyerId, Name = "Anonymous" },
            RequestId = new Guid().ToString(),
            Items = basket.Items,
            buyerBasketInfo = basketCheckout
        });

        return Accepted();
    }
}
