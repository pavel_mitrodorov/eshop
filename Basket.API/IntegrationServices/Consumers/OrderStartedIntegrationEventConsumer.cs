﻿namespace Basket.API.IntegrationServices.Consumers;

public class OrderStartedIntegrationEventConsumer
    : IConsumer<OrderStartedIntegrationEvent>
{
    private IBasketRepository _repository;
    public OrderStartedIntegrationEventConsumer(IBasketRepository basketRepository)
    {
        _repository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
    }
    public async Task Consume(ConsumeContext<OrderStartedIntegrationEvent> context)
    {
        await _repository.DeleteAsync(context.Message.UserId);
    }
}
