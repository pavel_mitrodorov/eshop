﻿namespace Basket.API.IntegrationServices.Consumers;

public class ProductPriceChangedIntegrationEventConsumer
    : IConsumer<ProductPriceChangedIntegrationEvent>
{
    private ILogger<ProductPriceChangedIntegrationEventConsumer> _logger;
    private IBasketRepository _basketRepository;
    public ProductPriceChangedIntegrationEventConsumer(ILogger<ProductPriceChangedIntegrationEventConsumer> logger, IBasketRepository basketRepository)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
    }

    public async Task Consume(ConsumeContext<ProductPriceChangedIntegrationEvent> context)
    {
        var userIDs = _basketRepository.GetAllBuyers();
        foreach(var userId in userIDs)
        {
            var basket = await _basketRepository.GetAsync(userId);
            await UpdatePriceInBasketItems(context.Message.ProductId, context.Message.NewPrice, basket);
        }
    }

    private async Task UpdatePriceInBasketItems(int productId, decimal newPrice, CustomerBasket basket)
    {
        var itemsToUpdate = basket?.Items?.Where(x => x.ProductId == productId).ToList();

        if (itemsToUpdate != null)
        {
            _logger.LogInformation("----- ProductPriceChangedIntegrationEventHandler - Updating items in basket for user: {BuyerId} ({@Items})", basket.BuyerId, itemsToUpdate);

            foreach (var item in itemsToUpdate)
            {
                item.UnitPrice = newPrice;
            }
            await _basketRepository.UpdateAsync(basket);
        }
    }
}
