﻿

namespace Health.UI.Controllers;

public class HomeController : Controller
{
    private IConfiguration _configuration;

    public HomeController(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public IActionResult Index()
    {
        return Redirect("/hc-ui");
    }

    public IActionResult Error()
    {
        return View();
    }

    [HttpGet("/Config")]
    public IActionResult Config()
    
    {
        var configurationValues = _configuration.GetSection("HealthChecksUI:HealthChecks")
            .GetChildren()
            .SelectMany(cs => cs.GetChildren())
            .Union(_configuration.GetSection("HealthChecks-UI:HealthChecks")
            .GetChildren()
            .SelectMany(cs => cs.GetChildren()))
            .ToDictionary(v => v.Path, v => v.Value);

        return View(configurationValues);
    }
}
