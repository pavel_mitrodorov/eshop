﻿global using Serilog;
global using Health.UI;
global using Microsoft.AspNetCore.Mvc;
global using HealthChecks.UI.Client;
global using Microsoft.AspNetCore.Diagnostics.HealthChecks;
