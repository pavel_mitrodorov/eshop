﻿namespace Ordering.Infrastructure;

public static class ModelBuilderInitExtension
{
    public static void AddInit(this ModelBuilder builder)
    {
        var cards = Enumeration.GetAll<CardType>();
        var states = Enumeration.GetAll<OrderStatus>();

        builder.Entity<CardType>().HasData(cards.ToArray());
        builder.Entity<OrderStatus>().HasData(states.ToArray());
    }
}
