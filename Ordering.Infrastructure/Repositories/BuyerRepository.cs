﻿namespace Ordering.Infrastructure.Repositories;

public class BuyerRepository
    : IBuyerRepository
{
    private readonly OrderingContext _context;
    public IUnitOfWork UnitOfWork
    {
        get
        {
            return _context;
        }
    }

    public BuyerRepository(OrderingContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task AddAsync(Buyer buyer, CancellationToken cancellationToken = default)
    {
        if (buyer.IsTransient())
        {
            await _context.Buyers.AddAsync(buyer, cancellationToken);
        }
    }

    public Buyer Update(Buyer buyer)
    {
        return _context.Buyers
                .Update(buyer)
                .Entity;
    }

    public async Task<Buyer> FindByUserIdentityAsync(string userIdentity, CancellationToken cancellationToken = default)
    {
        var buyer = await _context.Buyers
            .Include(b => b.PaymentMethods)
            .Where(b => b.UserIdentity == userIdentity)
            .SingleOrDefaultAsync(cancellationToken);

        return buyer;
    }

    public async Task<Buyer> FindByIdAsync(int id, CancellationToken cancellationToken = default)
    {
        var buyer = await _context.Buyers
            .Include(b => b.PaymentMethods)
            .Where(b => b.Id == id)
            .SingleOrDefaultAsync();

        return buyer;
    }
}
