﻿namespace Ordering.Infrastructure.Repositories;

public class OrderRepository
    : IOrderRepository
{
    private readonly OrderingContext _context;

    public IUnitOfWork UnitOfWork
    {
        get
        {
            return _context;
        }
    }

    public OrderRepository(OrderingContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task AddAsync(Order order, CancellationToken cancellationToken = default)
    {
        if (order.IsTransient())
        {
            await _context.Orders.AddAsync(order, cancellationToken);
        }
    }

    public async Task<Order> GetAsync(int orderId, CancellationToken cancellationToken = default)
    {
        var order = await _context
                            .Orders
                            .Include(x => x.Address)
                            .FirstOrDefaultAsync(o => o.Id == orderId, cancellationToken);
        if (order == null)
        {
            order = _context
                        .Orders
                        .Local
                        .FirstOrDefault(o => o.Id == orderId);
        }
        if (order != null)
        {
            await _context.Entry(order)
                .Collection(i => i.OrderItems).LoadAsync(cancellationToken);
            await _context.Entry(order)
                .Reference(i => i.OrderStatus).LoadAsync(cancellationToken);
        }

        return order;
    }

    public void Update(Order order)
    {
        _context.Entry(order).State = EntityState.Modified;
    }
}
