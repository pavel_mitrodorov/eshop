﻿namespace Ordering.Infrastructure.EntityConfigurations;

class PaymentMethodEntityTypeConfiguration
    : IEntityTypeConfiguration<PaymentMethod>
{
    public void Configure(EntityTypeBuilder<PaymentMethod> paymentConfiguration)
    {
        paymentConfiguration.ToTable("paymentmethods", OrderingContext.DEFAULT_SCHEMA);

        paymentConfiguration.HasKey(b => b.Id);

        paymentConfiguration.Ignore(b => b.DomainEvents);

        paymentConfiguration.Property(b => b.Id)
            .UseHiLo("paymentseq", OrderingContext.DEFAULT_SCHEMA);

        paymentConfiguration.Property("BuyerId")
            .IsRequired();

        paymentConfiguration
            .Property(p => p.CardHolderName)
            .HasMaxLength(200)
            .IsRequired();

        paymentConfiguration
            .Property(p => p.Alias)
            .HasMaxLength(200)
            .IsRequired();

        paymentConfiguration
            .Property(p => p.CardNumber)
            .HasMaxLength(25)
            .IsRequired();

        paymentConfiguration
            .Property(p => p.Expiration)
            .HasMaxLength(25)
            .IsRequired();

        paymentConfiguration
            .Property(p => p.CardTypeId)
            .IsRequired();

        paymentConfiguration.HasOne(p => p.CardType)
            .WithMany()
            .HasForeignKey(p => p.CardTypeId)
            .OnDelete(DeleteBehavior.Restrict);
    }
}
