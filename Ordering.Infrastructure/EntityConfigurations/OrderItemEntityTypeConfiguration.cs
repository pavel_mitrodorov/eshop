﻿namespace Ordering.Infrastructure.EntityConfigurations;

class OrderItemEntityTypeConfiguration
    : IEntityTypeConfiguration<OrderItem>
{
    public void Configure(EntityTypeBuilder<OrderItem> orderItemConfiguration)
    {
        orderItemConfiguration.ToTable("orderItems", OrderingContext.DEFAULT_SCHEMA);

        orderItemConfiguration.HasKey(o => o.Id);

        orderItemConfiguration.Ignore(b => b.DomainEvents);

        orderItemConfiguration.Property(o => o.Id)
            .UseHiLo("orderitemseq");

        orderItemConfiguration.Property<int>("OrderId")
            .IsRequired();

        orderItemConfiguration.Property(p => p.Discount)
            .IsRequired();

        orderItemConfiguration.Property(p => p.ProductId)
            .IsRequired();

        orderItemConfiguration.Property(p => p.ProductName)
            .IsRequired();

        orderItemConfiguration.Property(p => p.UnitPrice)
            .IsRequired();

        orderItemConfiguration.Property(p => p.Units)
            .IsRequired();
    }
}
