﻿namespace Ordering.Infrastructure.EntityConfigurations;

class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> orderConfiguration)
    {
        orderConfiguration.ToTable("orders", OrderingContext.DEFAULT_SCHEMA);

        orderConfiguration.HasKey(o => o.Id);

        orderConfiguration.Ignore(b => b.DomainEvents);

        orderConfiguration.Property(o => o.Id)
            .UseHiLo("orderseq", OrderingContext.DEFAULT_SCHEMA);

        orderConfiguration
            .OwnsOne(o => o.Address);

        orderConfiguration.Property(p => p.BuyerId)
            .IsRequired(false);

        orderConfiguration.Property(p => p.OrderDate)
            .IsRequired();

        orderConfiguration.Property(p => p.OrderStatusId)
            .IsRequired();

        orderConfiguration.Property(p => p.PaymentMethodId)
            .IsRequired(false);

        orderConfiguration.Property(p => p.Description)
            .IsRequired(false);

        orderConfiguration.HasOne<PaymentMethod>()
            .WithMany()
            .HasForeignKey(p => p.PaymentMethodId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.Restrict);

        orderConfiguration.HasOne<Buyer>()
            .WithMany()
            .IsRequired(false)
            .HasForeignKey(p => p.BuyerId)
            .OnDelete(DeleteBehavior.Restrict);

        //orderConfiguration.HasOne<OrderStatus>()
        //    .WithMany()
        //    .HasForeignKey(p => p.OrderStatusId)
        //    .OnDelete(DeleteBehavior.Restrict);
        //
        //orderConfiguration.HasMany<OrderItem>()
        //    .WithOne()
        //    .HasForeignKey("OrderId")
        //    .OnDelete(DeleteBehavior.Cascade);
    }
}
