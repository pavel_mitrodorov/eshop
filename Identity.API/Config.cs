﻿using IdentityServer4.Models;

namespace Identity.API;

public static class Config
{
    public static IEnumerable<ApiResource> GetApis() =>
        new List<ApiResource>
        {
            new ApiResource("basket", "Basket Service")
        };
    
    public static IEnumerable<IdentityResource> GetResources() =>
        new List<IdentityResource>
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile()
        };
    public static IEnumerable<Client> GetClients() =>
        new List<Client>
        {
            new Client
            {
                ClientId ="basketapi",
                ClientName = "Basket API",
                AllowedGrantTypes = GrantTypes.ClientCredentials,
                ClientSecrets =
                    {
                        new Secret("qaz".Sha256())
                    },
                AllowedScopes =
                    {
                        "basket"
                    }
            }
        };
}
