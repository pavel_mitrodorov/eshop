﻿using Microsoft.AspNetCore.Authentication;

namespace Identity.API.Controllers.V1;

[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]/[action]")]
public class AccountController : ControllerBase
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IConfiguration _config;
    public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration config)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _config = config;
    }

    [HttpPost]
    public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
    {
        var user = new ApplicationUser()
        {
            Email = registerViewModel.Email,
            UserName = registerViewModel.Email,
            City = registerViewModel.City,
            Country = registerViewModel.Country,
            Name = registerViewModel.Name,
            LastName = registerViewModel.LastName,
            State = registerViewModel.State,
            Street = registerViewModel.Street,
            SecurityStamp = Guid.NewGuid().ToString()
        };
        var result = await _userManager.CreateAsync(user, registerViewModel.Password);
        
        return result.Succeeded ? Ok("Register success") : BadRequest();
    }
    [HttpPost]
    public async Task<IActionResult> Login(LoginViewModel loginViewModel)
    {
        var user = await _userManager.FindByEmailAsync(loginViewModel.Email);
        if (user != null && await _userManager.CheckPasswordAsync(user, loginViewModel.Password))
        {
            var tokenLifetime = _config.GetValue("TokenLifetimeMinutes", 120);

            var props = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(tokenLifetime),
                AllowRefresh = true
            };
            await _signInManager.SignInAsync(user, props);
            return Ok();
        }
        else
        {
            return Unauthorized();  
        }
    }

    [HttpPost]
    public async Task<IActionResult> Logout()
    {
        await _signInManager.SignOutAsync();
        return Ok();
    }
}
