﻿namespace Identity.API.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
[ApiController]
[Route("api/[controller]/[action]")]
public class HomeController : ControllerBase
{

    [HttpGet]
    public IActionResult Index()
    {
        return Ok("indexpage");
    }
}
