﻿namespace Ordering.API.Infrastructure;
public enum CacheKey
{
    CardTypes,
    OrderStatuses
}
