﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Ordering.API.Infrastructure.Filters;
//Контроллеры, аннотированные [ApiController] атрибутом, автоматически проверяют состояние модели и возвращают ответ 400.
//https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-6.0#automatic-http-400-responses
// Что бы отключить было применено свойство SuppressModelStateInvalidFilter = true
public class ValidateModelStateFilter : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ModelState.IsValid)
        {
            return;
        }

        var validationErrors = context.ModelState
            .Keys
            .SelectMany(k => context.ModelState[k].Errors)
            .Select(e => e.ErrorMessage)
            .ToArray();

        var json = new ErrorViewModel(validationErrors, this.GetType().Name, "One or more validation errors occurred.");

        context.Result = new BadRequestObjectResult(json);
    }
}
