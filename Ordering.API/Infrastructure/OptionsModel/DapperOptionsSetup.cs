﻿namespace Ordering.API.Infrastructure.OptionsModel;

public class DapperOptionsSetup
{
    public string ConnectionString { get; set; }
}
