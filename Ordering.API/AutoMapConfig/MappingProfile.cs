﻿namespace Ordering.API.AutoMapConfig;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<OrderItem, OrderItemDto>();
        CreateMap<IntegrationEvents.Contracts.DTO.BasketItem, OrderItemDto>()
            .ForMember(dect => dect.Units,
                       opt => opt.MapFrom(src => src.Quantity));
    }
}
