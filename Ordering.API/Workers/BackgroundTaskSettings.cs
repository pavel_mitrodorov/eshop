﻿namespace Ordering.API.Workers;

public class BackgroundTaskSettings
{
    public int GracePeriodTime { get; set; }

    public int CheckUpdateTime { get; set; }
}
