﻿namespace Ordering.API.Workers;

public class GracePeriodManagerWorker : BackgroundService
{
    private readonly ILogger<GracePeriodManagerWorker> _logger;
    private readonly IBus _bus;
    private readonly BackgroundTaskSettings _settings;
    private readonly IOrderQueries _orderQueries;
    private readonly WorkerWitness _witness;
    public GracePeriodManagerWorker(
        ILogger<GracePeriodManagerWorker> logger,
        IBus bus,
        IOptions<BackgroundTaskSettings> settings,
        IOrderQueries orderQueries,
        WorkerWitness witness)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _bus = bus ?? throw new ArgumentNullException(nameof(bus));
        _settings = settings.Value ?? throw new ArgumentNullException(nameof(settings));
        _orderQueries = orderQueries ?? throw new ArgumentNullException(nameof(orderQueries));
        _witness = witness ?? throw new ArgumentNullException(nameof(witness));
    }
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogDebug("GracePeriodManagerService is starting");
        stoppingToken.Register(() => _logger.LogDebug("#1 GracePeriodManagerService background task is stopping"));
        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.LogDebug("GracePeriodManagerService background task is doing background work");
            try
            {
                var ids = await _orderQueries.GetConfirmedGracePeriodOrders(_settings.GracePeriodTime);
                foreach (var id in ids)
                {
                    await _bus.Publish<GracePeriodConfirmedIntegrationEvent>(new
                    {
                        OrderId = id
                    }, stoppingToken);
                }
            }
            catch (SqlException exp)
            {
                _logger.LogCritical(exp, "FATAL ERROR: Database connections could not be opened: {Message}", exp.Message);
            }
            catch (Exception exp)
            {
                _logger.LogCritical(exp, "FATAL ERROR on worker : {Message}", exp.Message);
            }

            _witness.LastExecution = DateTime.Now;
            await Task.Delay(_settings.CheckUpdateTime, stoppingToken);
        }

    }
}
