﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Ordering.API.Workers;

public class WorkerHealthCheck : IHealthCheck
{
    private readonly WorkerWitness _witness;
    private readonly int _differenceInSec;

    public WorkerHealthCheck(WorkerWitness witness, IOptions<BackgroundTaskSettings> options)
    {
        _witness = witness;
        _differenceInSec = options.Value.GracePeriodTime * 2;

    }

    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        return DateTime.Now.Subtract(_witness.LastExecution).TotalSeconds < _differenceInSec ?
                        Task.FromResult(HealthCheckResult.Healthy()) :
                        Task.FromResult(HealthCheckResult.Unhealthy());
    }
}
