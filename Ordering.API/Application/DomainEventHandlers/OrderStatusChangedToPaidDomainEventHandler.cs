﻿namespace Ordering.API.Application.DomainEventHandlers;

public class OrderStatusChangedToPaidDomainEventHandler
                : INotificationHandler<OrderStatusChangedToPaidDomainEvent>
{
    private readonly IOrderRepository _orderRepository;
    private readonly ILoggerFactory _logger;
    private readonly IBuyerRepository _buyerRepository;
    private readonly IPublishEndpoint _publishEndpoint;


    public OrderStatusChangedToPaidDomainEventHandler(
        IOrderRepository orderRepository, 
        ILoggerFactory logger,
        IBuyerRepository buyerRepository,
        IPublishEndpoint publishEndpoint
        )
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _buyerRepository = buyerRepository ?? throw new ArgumentNullException(nameof(buyerRepository));
        _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
    }

    public async Task Handle(OrderStatusChangedToPaidDomainEvent orderStatusChangedToPaidDomainEvent, CancellationToken cancellationToken)
    {
        _logger.CreateLogger<OrderStatusChangedToPaidDomainEventHandler>()
            .LogTrace("Order with Id: {OrderId} has been successfully updated to status {Status} ({Id})",
                orderStatusChangedToPaidDomainEvent.OrderId, nameof(OrderStatus.Paid), OrderStatus.Paid.Id);

        var order = await _orderRepository.GetAsync(orderStatusChangedToPaidDomainEvent.OrderId, cancellationToken);
        var buyer = await _buyerRepository.FindByIdAsync(order.BuyerId.GetValueOrDefault(), cancellationToken);

        await _publishEndpoint.Publish<OrderStatusChangedToPaidIntegrationEvent>(new
        {
            OrderId = order.Id,
            OrderStatus = order.OrderStatus.Name,
            BuyerName = buyer.Name,
            OrderStockItems = orderStatusChangedToPaidDomainEvent.OrderItems
                                .Select(p => new { ProductId = p.ProductId, Units = p.Units })
        }, cancellationToken);
    }
}
