﻿namespace Ordering.API.Application.DomainEventHandlers
{
    public class ValidateOrAddBuyerAggregateWhenOrderStartedDomainEventHandler
                        : INotificationHandler<OrderStartedDomainEvent>
    {
        private readonly IBuyerRepository _buyerRepository;
        private readonly IPublishEndpoint _publishEndpoint;

        public ValidateOrAddBuyerAggregateWhenOrderStartedDomainEventHandler(
            IBuyerRepository buyerRepository,
            IPublishEndpoint publishEndpoint)
        {
            _buyerRepository = buyerRepository ?? throw new ArgumentNullException(nameof(buyerRepository));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        }

        public async Task Handle(OrderStartedDomainEvent orderStartedEvent, CancellationToken cancellationToken)
        {
            var cardTypeId = (orderStartedEvent.CardTypeId != 0) ? orderStartedEvent.CardTypeId : 1;
            var buyer = await _buyerRepository.FindByUserIdentityAsync(orderStartedEvent.UserId, cancellationToken);
            bool buyerOriginallyExisted = (buyer == null) ? false : true;

            if (!buyerOriginallyExisted)
            {
                buyer = new Buyer(orderStartedEvent.UserId, orderStartedEvent.UserName);
            }

            buyer.VerifyOrAddPaymentMethod(cardTypeId,
                                            $"Payment Method on {DateTime.UtcNow}",
                                            orderStartedEvent.CardNumber,
                                            orderStartedEvent.CardSecurityNumber,
                                            orderStartedEvent.CardHolderName,
                                            orderStartedEvent.CardExpiration,
                                            orderStartedEvent.Order.Id);

            if (buyerOriginallyExisted)
            {
                _buyerRepository.Update(buyer);
            }
            else
            {
                await _buyerRepository.AddAsync(buyer, cancellationToken);
            }

            await _buyerRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);

            await _publishEndpoint.Publish<OrderStatusChangedToSubmittedIntegrationEvent>(new
            {
                OrderId = orderStartedEvent.Order.Id,
                //OrderStatus = OrderStatus.Submitted.ToString(),
                OrderStatus = orderStartedEvent.Order.OrderStatus.Name,
                BuyerName = buyer.Name
            }, cancellationToken);
        }
    }
}
