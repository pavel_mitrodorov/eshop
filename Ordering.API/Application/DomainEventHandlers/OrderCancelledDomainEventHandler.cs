﻿namespace Ordering.API.Application.DomainEventHandlers;

public class OrderCancelledDomainEventHandler
                : INotificationHandler<OrderCancelledDomainEvent>
{
    private readonly IBuyerRepository _buyerRepository;
    private readonly ILoggerFactory _logger;
    private readonly IPublishEndpoint _publishEndpoint;

    public OrderCancelledDomainEventHandler(
        ILoggerFactory logger,
        IBuyerRepository buyerRepository,
        IPublishEndpoint publishEndpoint)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _buyerRepository = buyerRepository ?? throw new ArgumentNullException(nameof(buyerRepository));
        _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
    }

    public async Task Handle(OrderCancelledDomainEvent orderCancelledDomainEvent, CancellationToken cancellationToken)
    {
        _logger.CreateLogger<OrderCancelledDomainEvent>()
            .LogTrace("Order with Id: {OrderId} has been successfully updated to status {Status} ({Id})",
                orderCancelledDomainEvent.Order.Id, nameof(OrderStatus.Cancelled), OrderStatus.Cancelled.Id);

        var buyer = await _buyerRepository.FindByIdAsync(orderCancelledDomainEvent.Order.BuyerId.GetValueOrDefault(), cancellationToken);
        await _publishEndpoint.Publish<OrderStatusChangedToCancelledIntegrationEvent>(new
        {
            OrderId = orderCancelledDomainEvent.Order.Id,
            OrderStatus = OrderStatus.Cancelled.ToString(),
            BuyerName = buyer.Name
        }, cancellationToken);
    }
}
