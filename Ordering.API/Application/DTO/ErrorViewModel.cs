﻿namespace Ordering.API.Application.DTO;

public class ErrorViewModel
{
    public string Title { get; set; }
    public string Code { get; set; }

    public string [] Errors { get; set; }
    public ErrorViewModel(string errorMessage, string title = "error")
    {
        this.Title = title;
        Errors = new [] { errorMessage };
    }
    public ErrorViewModel(string[] errorMessage, string title = "error")
    {
        this.Title = title;
        Errors = errorMessage;
    }
    public ErrorViewModel(string errorMessage, string code, string title = "error")
        : this(errorMessage, title)
    {
        Code = code;
    }
    public ErrorViewModel(string[] errorMessage, string code, string title = "error")
        :this(errorMessage, title)
    {
        Code = code;
    }
}
