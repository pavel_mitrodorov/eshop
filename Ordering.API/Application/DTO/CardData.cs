﻿namespace Ordering.API.Application.DTO;
public record CardData
{
    public string CardNumber { get; init; }
    public string CardHolderName { get; init; }
    public DateTime CardExpiration { get; init; }
    public string CardSecurityNumber { get; init; }
    public int CardTypeId { get; init; }
}
