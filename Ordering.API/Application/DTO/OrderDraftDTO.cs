﻿namespace Ordering.API.Application.DTO;

public class OrderDraftDto
{
    public IEnumerable<OrderItemDto> OrderItems { get; init; }
    public decimal Total { get; init; }

}
