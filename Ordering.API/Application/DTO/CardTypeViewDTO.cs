﻿namespace Ordering.API.Application.DTO;

public class CardTypeViewDto
{
    public int Id { get; set; }
    public string Name { get; set; }
}
