﻿namespace Ordering.API.Application.DTO
{
    public class OrderitemShortViewDto
    {
        public string Productname { get; set; }
        public int Units { get; set; }
        public double Unitprice { get; set; }
        public string Pictureurl { get; set; }
    }
}
