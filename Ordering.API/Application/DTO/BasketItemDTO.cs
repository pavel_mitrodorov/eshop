﻿namespace Ordering.API.Application.DTO;

public class BasketItemDto
{
    [Required(ErrorMessage = "Не указано Id")]
    public string Id { get; init; }

    [Required(ErrorMessage = "Не указано ProductId")]
    public int ProductId { get; init; }

    [Required(ErrorMessage = "Не указано ProductName")]
    [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
    public string ProductName { get; init; }

    [Required(ErrorMessage = "Не указано UnitPrice")]
    [Range(0, double.MaxValue, ErrorMessage = "Недопустимая цена")]
    public decimal UnitPrice { get; init; }

    public decimal OldUnitPrice { get; init; }

    [Required(ErrorMessage = "Не указано Quantity")]
    [Range(0, double.MaxValue, ErrorMessage = "Недопустимое кол-во")]
    public int Quantity { get; init; }

    public string PictureUrl { get; init; }
}
