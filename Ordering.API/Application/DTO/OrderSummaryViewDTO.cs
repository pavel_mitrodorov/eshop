﻿namespace Ordering.API.Application.DTO;

public class OrderSummaryViewDto
{
    public int Ordernumber { get; set; }
    public DateTime Date { get; set; }
    public string Status { get; set; }
    public double Total { get; set; }
}
