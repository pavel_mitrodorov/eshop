﻿using Dapper;
using System.Data;

namespace Ordering.API.Application.Queries;

public class OrderQueries
    : IOrderQueries
{
    private readonly string _connectionString;

    public OrderQueries(string connectionString)
    {
        _connectionString = connectionString;
    }

    public async Task<IEnumerable<int>> GetConfirmedGracePeriodOrders(int gracePeriodTime, CancellationToken cancellationToken = default)
    {
        using (IDbConnection db = new SqlConnection(_connectionString))
        {
            return await db.QueryAsync<int>(
                        sql: @"select 
                                   Id 
                                 from [ordering].[orders] 
                                 where datediff(minute, [OrderDate], getdate()) >= @GracePeriodTime
                                   and [OrderStatusId] = 1",
                        param: new {GracePeriodTime = gracePeriodTime });
        }
    }

    public async Task<IEnumerable<CardTypeViewDto>> GetCardTypesAsync()
    {

        using (IDbConnection db = new SqlConnection(_connectionString))
        {
            return await db.QueryAsync<CardTypeViewDto>(
                "select " +
                    "o.Id   as [Id]," +
                    "o.Name as [Name]" +
                  "FROM ordering.cardtypes as o"
            );
        }
        
    }

    public async Task<OrderViewDto> GetAsync(int id)
    {
        var orderresult = new OrderViewDto();
        using (IDbConnection db = new SqlConnection(_connectionString))
        {
            string sql =
                @"select
                       o.Id                    as [Ordernumber]
                      ,o.OrderDate             as [Date]
                      ,o.Description           as [Description]
                      ,o.Address_City          as [City]
                      ,o.Address_Country       as [Country]
                      ,o.Address_State         as [State]
                      ,o.Address_Street        as [Street]
                      ,o.Address_ZipCode       as [Zipcode]
                      ,s.Name                  as [Status]
                    from ordering.Orders           as o
                    left join ordering.orderstatus as s on s.Id = o.OrderStatusId
                    WHERE o.Id = @id

                select
                     i.ProductName           as [Productname]
                    ,i.Units                 as [Units]
                    ,i.UnitPrice             as [Unitprice]
                    ,i.PictureUrl            as [Pictureurl]
                  from ordering.Orderitems as i
                  where i.orderid = @id";

            using (var multi = await db.QueryMultipleAsync(sql, new { id }))
            {
                orderresult = multi.Read<OrderViewDto>().Single();
                orderresult.Orderitems = multi.Read<OrderitemShortViewDto>().ToList();
            }

            return orderresult;
        }
    }

    public async Task<IEnumerable<OrderSummaryViewDto>> GetOrdersFromUserAsync(string userId)
    {
        using (IDbConnection db = new SqlConnection(_connectionString))
        {
            return await db.QueryAsync<OrderSummaryViewDto>(
                 sql: @"select 
                            o.[Id]                       as [ordernumber]
                           ,o.[OrderDate]                as [date]
                           ,s.[Name]                     as [status]
                           ,sum(i.units * i.unitprice)   as [total]
                         from [ordering].[Orders] as o
                         left join [ordering].[orderitems]  as i ON i.orderid = o.Id
                         left join [ordering].[orderstatus] as s on s.Id = o.OrderStatusId                      
                         left join [ordering].[buyers]      as b on b.Id = o.BuyerId
                         where b.UserIdentity = @userId
                         group by 
                            o.[Id]
                           ,o.[OrderDate]
                           ,s.[Name] 
                         order by 
                           o.[Id]"
                ,param: new { userId }
            );
        }
    }
}
