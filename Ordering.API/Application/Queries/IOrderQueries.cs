﻿namespace Ordering.API.Application.Queries;

public interface IOrderQueries
{
    Task<OrderViewDto> GetAsync(int id);

    Task<IEnumerable<OrderSummaryViewDto>> GetOrdersFromUserAsync(string userId);

    Task<IEnumerable<CardTypeViewDto>> GetCardTypesAsync();

    Task<IEnumerable<int>> GetConfirmedGracePeriodOrders(int gracePeriodTime, CancellationToken cancellationToken = default);
}
