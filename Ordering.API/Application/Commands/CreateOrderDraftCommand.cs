﻿namespace Ordering.API.Application.Commands;

public class CreateOrderDraftCommand : IRequest<OrderDraftDto>
{
    [Required(ErrorMessage = "Не указано BuyerId")]
    public int BuyerId { get; private set; }

    [Required(ErrorMessage = "Не указано Items")]
    public IEnumerable<BasketItemDto> Items { get; private set; }

    public CreateOrderDraftCommand(int buyerId, IEnumerable<BasketItemDto> items)
    {
        BuyerId = buyerId;
        Items = items;
    }
}

