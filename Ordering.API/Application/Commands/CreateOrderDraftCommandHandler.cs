﻿namespace Ordering.API.Application.Commands;

public class CreateOrderDraftCommandHandler
    : IRequestHandler<CreateOrderDraftCommand, OrderDraftDto>
{
    private readonly IMapper _mapper;

    public CreateOrderDraftCommandHandler(IMapper mapper)
    {
        _mapper = mapper;
    }

    private OrderDraftDto FromOrder(Order order)
    {
        return new OrderDraftDto()
        {
            OrderItems = _mapper.Map<List<OrderItemDto>>(order.OrderItems),
            Total = order.GetTotal()
        };
    }

    public Task<OrderDraftDto> Handle(CreateOrderDraftCommand message, CancellationToken cancellationToken)
    {
        var order = Order.NewDraft();
        order.SetBuyerId(message.BuyerId);
        foreach (var item in message.Items)
        {
            order.AddOrderItem(item.ProductId, item.ProductName, item.UnitPrice, 0, item.PictureUrl, item.Quantity);
        }
        return Task.FromResult(FromOrder(order));
    }
}
