﻿namespace Ordering.API.Application.Commands;

public class CreateOrderCommand
    : IRequest<bool>
{
    private readonly List<OrderItemDto> _orderItems;
    public string UserId { get; private set; }
    public string UserName { get; private set; }
    public string City { get; private set; }
    public string Street { get; private set; }
    public string State { get; private set; }
    public string Country { get; private set; }
    public string ZipCode { get; private set; }
    public string CardNumber { get; private set; }
    public string CardHolderName { get; private set; }
    public DateTime CardExpiration { get; private set; }
    public string CardSecurityNumber { get; private set; }
    public int CardTypeId { get; private set; }
    public IReadOnlyCollection<OrderItemDto> OrderItems => _orderItems.AsReadOnly();

    public CreateOrderCommand()
    {
        _orderItems = new List<OrderItemDto>();
    }

    public CreateOrderCommand(List<OrderItemDto> orderItems, string userId, string userName, string city, string street, string state, string country, string zipcode,
        string cardNumber, string cardHolderName, DateTime cardExpiration,
        string cardSecurityNumber, int cardTypeId) : this()
    {
        _orderItems = orderItems;
        UserId = userId;
        UserName = userName;
        City = city;
        Street = street;
        State = state;
        Country = country;
        ZipCode = zipcode;
        CardNumber = cardNumber;
        CardHolderName = cardHolderName;
        CardExpiration = cardExpiration;
        CardSecurityNumber = cardSecurityNumber;
        CardTypeId = cardTypeId;
        CardExpiration = cardExpiration;
    }
}
