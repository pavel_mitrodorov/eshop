﻿namespace Ordering.API.Application.Commands;

public class SetPaidOrderStatusCommand : IRequest<bool>
{
    public int OrderNumber { get; private set; }
    public SetPaidOrderStatusCommand(int orderNumber)
    {
        OrderNumber = orderNumber;
    }
}
