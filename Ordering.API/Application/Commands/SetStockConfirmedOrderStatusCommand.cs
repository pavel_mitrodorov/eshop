﻿namespace Ordering.API.Application.Commands;

public class SetStockConfirmedOrderStatusCommand : IRequest<bool>
{
    public int OrderNumber { get; private set; }

    public SetStockConfirmedOrderStatusCommand(int orderNumber)
    {
        OrderNumber = orderNumber;
    }
}
