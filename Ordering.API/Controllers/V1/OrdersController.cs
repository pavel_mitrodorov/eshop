﻿namespace Ordering.API.Controllers.V1;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
public class OrdersController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IIdentityService _identityService;
    private readonly IOrderQueries _orderQueries;
    private readonly IAppCache _cache;

    public OrdersController(
    IMediator mediator,
    IIdentityService identityService,
    IAppCache cache,
    IOrderQueries orderQueries)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
        _orderQueries = orderQueries ?? throw new ArgumentNullException(nameof(orderQueries));
        _cache = cache ?? throw new ArgumentNullException(nameof(cache));
    }

    /// <summary>отмена заказа</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="orderId">id заказа</param>
    /// <returns>Ok result</returns>

    [Route("{orderId:int:min(0)}/cancel")]
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> CancelAsync([FromRoute] int orderId, CancellationToken cancellationToken)
    {
        bool commandResult = await _mediator.Send(new CancelOrderCommand(orderId), cancellationToken);
        if (!commandResult)
        {
            return BadRequest(new ErrorViewModel($"Не удалось отменить заказ {orderId}"));
        }


        return Ok();
    }

    /// <summary>заказ доставлен</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="orderId">id заказа</param>
    /// <returns>Ok result</returns>
    [Route("{orderId:int:min(0)}/ship")]
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> ShipAsync([FromRoute] int orderId, CancellationToken cancellationToken)
    {
        bool commandResult = await _mediator.Send(new ShipOrderCommand(orderId), cancellationToken);
        if (!commandResult)
        {
            return BadRequest(new ErrorViewModel($"Не удалось установить заказ {orderId} в статус \"доставлен\""));
        }

        return Ok();
    }

    /// <summary>создание чернового заказа</summary>
    /// <param name="create">тело запроса</param>
    /// <param name="cancellationToken">cancellationToken</param>
    /// <returns>заказ</returns>
    [Route("draft")]
    [HttpPost]
    [ProducesResponseType(typeof(OrderDraftDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> CreateDraftFromBasketDataAsync([FromBody] CreateOrderDraftCommand create, CancellationToken cancellationToken)
    {
        var res =  await _mediator.Send(create, cancellationToken);
        if (res == null)
        {
            return BadRequest(new ErrorViewModel("Не удалось создать черновик заказа!"));
        }
        return Ok(res);
    }

    /// <summary>заказ</summary>
    /// <param name="orderId">id заказа</param>
    /// <returns>Ok result</returns>
    [Route("{orderId:int:min(0)}")]
    [HttpGet]
    [ProducesResponseType(typeof(OrderViewDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAsync([FromRoute] int orderId)
    {
        var order = await _orderQueries.GetAsync(orderId);
        if (order == null)
        {
            return NotFound($"заказ {orderId} не найден!");
        }
        return Ok(order);
    }

    /// <summary>заказы тек. пользователя</summary>
    /// <returns>список заказов</returns>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<OrderSummaryViewDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetUserOrdersAsync()
    {
        var userid = _identityService.GetUserIdentity();
        var orders = await _orderQueries.GetOrdersFromUserAsync(userid);
        return Ok(orders);

    }

    /// <summary>типы банковских карт</summary>
    /// <returns>список заказов</returns>
    [Route("cardtypes")]
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<CardType>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetCardTypesAsync()
    {
        var model = await _cache.GetOrAdd(
            CacheKey.CardTypes.ToString(),
            () => _orderQueries.GetCardTypesAsync(),
            new MemoryCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = new TimeSpan(8, 0, 0)
            });
        return Ok(model);
    }
}
