﻿namespace Ordering.API.IntegrationServices.Consumers;

public class GracePeriodConfirmedIntegrationEventConsumer
    : IConsumer<GracePeriodConfirmedIntegrationEvent>
{
    private readonly IMediator _mediator;

    public GracePeriodConfirmedIntegrationEventConsumer(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Consume(ConsumeContext<GracePeriodConfirmedIntegrationEvent> context)
    {
        var command = new SetAwaitingValidationOrderStatusCommand(context.Message.OrderId);
        await _mediator.Send(command, context.CancellationToken);
    }
}
