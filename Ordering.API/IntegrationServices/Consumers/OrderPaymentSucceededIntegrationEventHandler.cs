﻿namespace Ordering.API.IntegrationServices.Consumers;

public class OrderPaymentSucceededIntegrationEventHandler
    : IConsumer<OrderPaymentSucceededIntegrationEvent>
{
    private readonly IMediator _mediator;
    public OrderPaymentSucceededIntegrationEventHandler(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }
    public async Task Consume(ConsumeContext<OrderPaymentSucceededIntegrationEvent> context)
    {
        var command = new SetPaidOrderStatusCommand(context.Message.OrderId);
        await _mediator.Send(command, context.CancellationToken);
    }
}
