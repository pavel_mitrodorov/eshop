﻿namespace Ordering.API.IntegrationServices.Consumers;

public class OrderPaymentFailedIntegrationEventHandler
    : IConsumer<OrderPaymentFailedIntegrationEvent>
{
    private readonly IMediator _mediator;

    public OrderPaymentFailedIntegrationEventHandler(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    public async Task Consume(ConsumeContext<OrderPaymentFailedIntegrationEvent> context)
    {
        var command = new CancelOrderCommand(context.Message.OrderId);
        await _mediator.Send(command, context.CancellationToken);
    }
}
