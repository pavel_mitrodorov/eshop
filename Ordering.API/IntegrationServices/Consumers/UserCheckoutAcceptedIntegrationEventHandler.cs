﻿namespace Ordering.API.IntegrationServices.Consumers;

public class UserCheckoutAcceptedIntegrationEventHandler
    : IConsumer<UserCheckoutAcceptedIntegrationEvent>
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;
    private ICardData _cardData;
    public UserCheckoutAcceptedIntegrationEventHandler(IMediator mediator, IMapper mapper, ICardData cardData)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _cardData = cardData ?? throw new ArgumentNullException(nameof(cardData));
    }

    public async Task Consume(ConsumeContext<UserCheckoutAcceptedIntegrationEvent> context)
    {
        var card = _cardData.Requsert(context.Message.BuyerBasketInfo.CardBindingId);

        var command = new CreateOrderCommand(
            _mapper.Map<List<OrderItemDto>>(context.Message.Items),
            context.Message.Buyer.Id,
            context.Message.Buyer.Name,
            context.Message.BuyerBasketInfo.City,
            context.Message.BuyerBasketInfo.Street,
            context.Message.BuyerBasketInfo.State,
            context.Message.BuyerBasketInfo.Country,
            context.Message.BuyerBasketInfo.ZipCode,
            card.CardNumber,
            card.CardHolderName,
            card.CardExpiration,
            card.CardSecurityNumber,
            card.CardTypeId

        );
        await _mediator.Send(command, context.CancellationToken);
    }

}
