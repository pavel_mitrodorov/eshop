﻿namespace Ordering.API.IntegrationServices.Consumers;

public class OrderStockConfirmedIntegrationEventHandler
    : IConsumer<OrderStockConfirmedIntegrationEvent>
{
    private readonly IMediator _mediator;

    public OrderStockConfirmedIntegrationEventHandler(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    public async Task Consume(ConsumeContext<OrderStockConfirmedIntegrationEvent> context)
    {
        var command = new SetStockConfirmedOrderStatusCommand(context.Message.OrderId);
        await _mediator.Send(command, context.CancellationToken);
    }
}
