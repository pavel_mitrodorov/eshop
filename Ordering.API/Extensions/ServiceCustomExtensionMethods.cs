﻿namespace Ordering.API.Extensions;

public static class ServiceCustomExtensionMethods
{
    public static IServiceCollection AddCustomMediatR(this IServiceCollection services)
    {
        services
            .AddMediatR(typeof(Startup))
            .AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>))
            .AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionBehaviour<,>));
        return services;
    }

    public static IServiceCollection AddCustomServices(this IServiceCollection services)
    {
        services
            .AddTransient<IIdentityService, IdentityService>()
            .AddSingleton<ICardData, CardDataRequest>();
        return services;
    }
    public static IServiceCollection AddAutoMappper(this IServiceCollection services)
    {
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        services.AddSingleton(mapperConfig.CreateMapper());
        return services;
    }
    public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddMassTransit(x =>
            {
                x.AddConsumer<GracePeriodConfirmedIntegrationEventConsumer>();
                x.AddConsumer<OrderPaymentFailedIntegrationEventHandler>();
                x.AddConsumer<OrderPaymentSucceededIntegrationEventHandler>();
                x.AddConsumer<OrderStockConfirmedIntegrationEventHandler>();
                x.AddConsumer<UserCheckoutAcceptedIntegrationEventHandler>();
                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["RabbitMQ:host"], configurator =>
                    {
                        configurator.Username(configuration["RabbitMQ:user"]);
                        configurator.Password(configuration["RabbitMQ:password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                    cfg.UseMessageRetry(r => r.Immediate(3));
                });
            });
        return services;
    }
    public static IServiceCollection AddCustomMvc(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddControllers(options =>
            {
                options.SuppressAsyncSuffixInActionNames = false;
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));
            })
            .ConfigureApiBehaviorOptions(op =>
                op.SuppressModelStateInvalidFilter = true
            )
            .AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);

        return services;
    }

    public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<WorkerWitness>();
        var hcBuilder = services.AddHealthChecks();

        hcBuilder
            .AddCheck<WorkerHealthCheck>("GracePeriodManagerWorker");

        hcBuilder
            .AddSqlServer(
                configuration["ConnectionString"],
                name: "CatalogDB-check",
                tags: new [] { "catalogdb" });
        //.AddRabbitMQ() -- HealthCheck на eventbus уже включен по умолчанию в MassTransit
        return services;
    }

    public static IServiceCollection AddCustomDbContext<TContext>(this IServiceCollection services, IConfiguration configuration) where TContext : DbContext
    {
        services.AddDbContext<TContext>(options =>
        {
            options.UseSqlServer(configuration["ConnectionString"],
                                 sqlServerOptionsAction: sqlOptions =>
                                 {
                                     sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                                     sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                                 }


            );
        });
        var sp = services.BuildServiceProvider();
        var context = sp.GetService<TContext>();
        var logger = sp.GetRequiredService<ILogger<TContext>>();
        if (context != null)
        {
            try
            {
                logger.LogInformation("Migrating database associated with context {DbContextName}", typeof(TContext).Name);
                context.Database.Migrate();
                logger.LogInformation("Migrated database associated with context {DbContextName}", typeof(TContext).Name);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred while migrating the database used on context {DbContextName}", typeof(TContext).Name);
            }

        }

        services.AddScoped<IOrderRepository, OrderRepository>();
        services.AddScoped<IBuyerRepository, BuyerRepository>();

        services.AddTransient<IOrderQueries>(
        provider =>
            {
                return new OrderQueries(configuration["ConnectionString"]);
            }
        );

        return services;
    }
}
