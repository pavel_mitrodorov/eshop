﻿namespace Payment.API.IntegrationServices.Consumers;

public class OrderStatusChangedToStockConfirmedIntegrationEventConsumer
    : IConsumer<OrderStatusChangedToStockConfirmedIntegrationEvent>
{
    private readonly IConfiguration _configuration;
    private IPublishEndpoint _endpoint;

    public OrderStatusChangedToStockConfirmedIntegrationEventConsumer(IConfiguration configuration, IPublishEndpoint endpoint)
    {
        _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        _endpoint = endpoint ?? throw new ArgumentNullException(nameof(endpoint));
    }

    public async Task Consume(ConsumeContext<OrderStatusChangedToStockConfirmedIntegrationEvent> context)
    {
        bool paymentSucceeded = Convert.ToBoolean(_configuration["PaymentSucceeded"]); 
        if (paymentSucceeded)
        {
            await _endpoint.Publish<OrderPaymentSucceededIntegrationEvent>(new
            {
                OrderId = context.Message.OrderId
            }, context.CancellationToken);
        }
        else
        {
            await _endpoint.Publish<OrderPaymentFailedIntegrationEvent>(new
            {
                OrderId = context.Message.OrderId
            }, context.CancellationToken);
        }
    }
}
