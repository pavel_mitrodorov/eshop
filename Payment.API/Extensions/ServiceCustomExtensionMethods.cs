﻿namespace Payment.API.Extensions;

public static class ServiceCustomExtensionMethods
{
    public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddMassTransit(x =>
            {
                x.AddConsumer<OrderStatusChangedToStockConfirmedIntegrationEventConsumer>();
                
                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["RabbitMQ:host"], configurator =>
                    {
                        configurator.Username(configuration["RabbitMQ:user"]);
                        configurator.Password(configuration["RabbitMQ:password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                    cfg.UseMessageRetry(r => r.Immediate(3));
                });
            });
        return services;
    }

    public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services)
    {
        services.AddHealthChecks();
        //.AddRabbitMQ() -- HealthCheck на eventbus уже включен по умолчанию в MassTransit
        return services;
    }
}
