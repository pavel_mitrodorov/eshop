﻿global using Payment.API;
global using Serilog;
global using MassTransit;
global using HealthChecks.UI.Client;
global using Microsoft.AspNetCore.Diagnostics.HealthChecks;
global using IntegrationEvents.Contracts.Event;
global using Payment.API.IntegrationServices.Consumers;