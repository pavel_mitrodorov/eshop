using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;

var configuration = GetConfiguration();
Log.Logger = CreateSerilogLogger();
try
{
    Log.Information("Configuring web host ({ApplicationContext})...", Program.AppName);
    var host = CreateHostBuilder(args).Build();

    Log.Information("Starting web host ({ApplicationContext})...", Program.AppName);
    host.Run();

    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", Program.AppName);
    return 1;
}
finally
{
    Log.CloseAndFlush();
}

IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder =>
        {
            var port = configuration.GetValue("PORT", 80);
            webBuilder.ConfigureKestrel(opt =>
            {
                opt.Listen(IPAddress.Any, port, listenOptions =>
                {
                    listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                });
            });
            webBuilder.UseStartup<Startup>();
            webBuilder.ConfigureAppConfiguration(c => c.AddConfiguration(configuration));
        })

        .UseSerilog();

IConfiguration GetConfiguration()
{
    var config = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables()
        .Build();
    return config;
}

Serilog.ILogger CreateSerilogLogger()
{
    return new LoggerConfiguration()
        .MinimumLevel.Debug()
        .Enrich.WithProperty("ApplicationContext", Program.AppName)
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .ReadFrom.Configuration(configuration)
        .CreateLogger();

}


public partial class Program
{
    public static string Namespace = typeof(Startup).Namespace ?? "default";
    public static string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);
}