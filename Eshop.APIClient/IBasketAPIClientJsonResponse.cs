﻿using Eshop.APIClient.Models;
using Refit;

namespace Eshop.APIClient;

public partial interface IBasketApiClient
{
    [Get("/api/v1/basket/")]
    Task<string> GetJsonResultAsync(CancellationToken cancellationToken = default);

    [Post("/api/v1/basket/")]
    Task<string> UpdateJsonResultAsync([Body] List<BasketItemDto> items, CancellationToken cancellationToken = default);

}
