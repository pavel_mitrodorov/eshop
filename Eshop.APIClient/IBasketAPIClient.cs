﻿using Eshop.APIClient.Models;
using Refit;

namespace Eshop.APIClient;

public partial interface IBasketApiClient
{
    [Get("/api/v1/basket/")]
    Task<CustomerBasketResponse> GetAsync(CancellationToken cancellationToken = default);

    [Post("/api/v1/basket/")]
    Task<CustomerBasketResponse> UpdateAsync([Body] List<BasketItemDto> items, CancellationToken cancellationToken = default);

}
