﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using Refit;

namespace Eshop.APIClient.Extensions;

public static class CustomServiceExtension
{
    public static void AddApiClient<T>(this IServiceCollection services, Action<HttpClientOptionsSetup> setup)
        where T : class
    {
        PolicyHttpClientOptionsSetup policyConfig = new PolicyHttpClientOptionsSetup();
        HttpClientOptionsSetup config = new HttpClientOptionsSetup();
        setup.Invoke(config);
        if (config.PolicySetup != null && config.ApplyPolicy)
        {
            config.PolicySetup.Invoke(policyConfig);
        }
        else if(config.ApplyPolicy)
        {
            config.PolicySetup = (p) =>
            {
                p = new PolicyHttpClientOptionsSetup();
            };
            config.PolicySetup.Invoke(policyConfig);
        }

        var refitClient = services
            .AddRefitClient<T>()
            .ConfigureHttpClient(c =>
            {
                c.BaseAddress = config.BaseUrl;
                c.Timeout = config.HttpClientTimeOut; //общий Timeout HttpClient
            });
            
        if (config.ConfigureHandler != null)
        {
            refitClient
                .AddHttpMessageHandler(config.ConfigureHandler);
        }
        if (config.ApplyPolicy)
        {
            var timeoutPolicy = Policy.TimeoutAsync<HttpResponseMessage>(policyConfig.PolicyTimeOutInSec);
            refitClient
                ////Если ответ от I...Client будет 
                ////Network failures(System.Net.Http.HttpRequestException)
                ////HTTP 5XX status codes(server errors)
                ////HTTP 408 status code(request timeout)
                //// Тогда будет сделан перезапрос 3 раза через указанный интервал времени 
                .AddPolicyHandler((services, request) => HttpPolicyExtensions.HandleTransientHttpError().Or<TimeoutRejectedException>()
                    .WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromMilliseconds(500),
                        TimeSpan.FromMilliseconds(1000),
                        TimeSpan.FromMilliseconds(2000)
                    },
                    //Логирование повторного запроса 
                    onRetry: (outcome, timespan, retryAttempt, context) =>
                    {

                        services.GetService<ILogger<T>>()?
                            .LogWarning("Polly making retry {retry} delaying for {delay}ms. Request info {request}. Exception message {message}", retryAttempt, timespan.TotalMilliseconds, request, outcome.Exception.Message);
                    }
                ))
                // Если в течении n секунд не получаем ответ, пробуем сделать еще 3 попытки через указанный интервал времени. При этом учитывается общий Timeout HttpClient
                .AddPolicyHandler(timeoutPolicy);
        }
    }

    public static IServiceCollection AddOrderApiClient(this IServiceCollection services, Action<HttpClientOptionsSetup> setup)
    {
        services
            .AddApiClient<IOrderApiClient>(setup);
        return services;
    }

    public static IServiceCollection AddCatalogApiClient(this IServiceCollection services, Action<HttpClientOptionsSetup> setup)
    {
        services
            .AddApiClient<ICatalogApiClient>(setup);
        return services;
    }

    public static IServiceCollection AddBasketApiClient(this IServiceCollection services, Action<HttpClientOptionsSetup> setup)
    {
        services
            .AddApiClient<IBasketApiClient>(setup);
        return services;
    }
}
