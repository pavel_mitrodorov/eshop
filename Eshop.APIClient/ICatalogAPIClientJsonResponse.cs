﻿using Refit;

namespace Eshop.APIClient;

public partial interface ICatalogApiClient
{
    [Get("/api/v1/catalog/items/{id}/")]
    Task<string> GetJsonResultAsync([AliasAs("id")] int id, CancellationToken cancellationToken = default);
}
