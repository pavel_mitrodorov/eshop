﻿namespace Eshop.APIClient.Models;

public class CustomerBasketResponse
{
    public string BuyerId { get; set; }
    public List<BasketItemDto> Items { get; set; }
}