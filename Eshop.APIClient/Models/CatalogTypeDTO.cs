﻿namespace Eshop.APIClient.Models
{
    public class CatalogTypeDto
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}
