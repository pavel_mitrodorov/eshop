﻿namespace Eshop.APIClient.Models;

public class CreateOrderDraftRequest
{
    public int BuyerId { get; set; }

    public IEnumerable<BasketItemDto> Items { get; set; }
}
