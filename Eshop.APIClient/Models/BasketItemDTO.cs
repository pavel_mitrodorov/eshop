﻿namespace Eshop.APIClient.Models;

public class BasketItemDto
{
    public string Id { get; set; }
    public int ProductId { get; set; }
    public string ProductName { get; set; }
    public decimal UnitPrice { get; set; }
    public decimal Quantity { get; set; }
    public string PictureUrl { get; set; }
}
