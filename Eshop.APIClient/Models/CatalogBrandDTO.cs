﻿namespace Eshop.APIClient.Models;

public class CatalogBrandDto
{
    public int Id { get; set; }

    public string Brand { get; set; }
}
