﻿namespace Eshop.APIClient;

public class HttpClientOptionsSetup
{
    
    private const double _httpClientTimeOut = 50;
    public Uri BaseUrl { get; set; }
    public TimeSpan HttpClientTimeOut { get; set; } = TimeSpan.FromSeconds(_httpClientTimeOut);
    public bool ApplyPolicy { get; set; } = true;
    public Action<PolicyHttpClientOptionsSetup> PolicySetup { get; set; }
    public Func<DelegatingHandler> ConfigureHandler { get; set; } = null;
}
