﻿namespace Eshop.APIClient;

public class PolicyHttpClientOptionsSetup
{
    private const int _policyTimeOutInSec = 15;
    public int PolicyTimeOutInSec { get; set; } = _policyTimeOutInSec;
}
