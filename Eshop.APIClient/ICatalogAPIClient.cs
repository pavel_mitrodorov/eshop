﻿using Eshop.APIClient.Models;
using Refit;

namespace Eshop.APIClient;

public partial interface ICatalogApiClient
{
    [Get("/api/v1/catalog/items/{id}/")]
    Task<CatalogItemResponse> GetAsync([AliasAs("id")] int id, CancellationToken cancellationToken = default);
}
