﻿using Eshop.APIClient.Models;
using Refit;

namespace Eshop.APIClient;

public partial interface IOrderApiClient
{
    [Get("/api/v1/orders/{id}/")]
    Task<OrderViewResponse> GetAsync([AliasAs("id")] int orderId, CancellationToken cancellationToken = default);

    [Post("/api/v1/orders/draft/")]
    Task<OrderViewResponse> CreateDraftFromBasketDataAsync([Body] CreateOrderDraftRequest createOrderDraft, CancellationToken cancellationToken = default);
}
