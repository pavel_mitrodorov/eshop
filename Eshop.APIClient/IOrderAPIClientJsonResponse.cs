﻿using Eshop.APIClient.Models;
using Refit;

namespace Eshop.APIClient;

public partial interface IOrderApiClient
{
    [Get("/api/v1/orders/{id}/")]
    Task<string> GetJsonResultAsync([AliasAs("id")] int orderId, CancellationToken cancellationToken = default);

    [Post("/api/v1/orders/draft/")]
    Task<string> CreateDraftFromBasketDataJsonResultAsync([Body] CreateOrderDraftRequest createOrderDraft, CancellationToken cancellationToken = default);
}
