# Eshop
Данный проект является аналогом "eShopOnContainers", но с собственными изменениями.
https://github.com/dotnet-architecture/eShopOnContainers
Пример эталонного приложения .NET Core от Microsoft, основанного на упрощенной архитектуре микрослужб и контейнерах Docker.
Подробное описание можно увидеть тут: https://docs.microsoft.com/ru-ru/dotnet/architecture/microservices/

Данный проект, предусмотрен исключительно как «pet-проект».
