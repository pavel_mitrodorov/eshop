﻿namespace Web.Shopping.HttpAggregator.ConfigurationModel;

public class ApiClientInfo
{
    public string HostName { get; set; }
    public string HcHostName { get; set; }
    public int TimeOutInSec { get; set; }
}
