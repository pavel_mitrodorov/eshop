﻿namespace Web.Shopping.HttpAggregator.ConfigurationModel;

public class ApiClients
{
    public ApiClientInfo Basket { get; set; }
    public ApiClientInfo Orders { get; set; }
    public ApiClientInfo Catalog { get; set; }
}
