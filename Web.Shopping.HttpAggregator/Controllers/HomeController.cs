﻿namespace Web.Shopping.HttpAggregator.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
public class HomeController : ControllerBase
{
    public IActionResult Index()
    {
        return new RedirectResult("~/swagger");
    }
}
