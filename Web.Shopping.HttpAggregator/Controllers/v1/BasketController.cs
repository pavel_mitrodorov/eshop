﻿

namespace Web.Shopping.HttpAggregator.Controllers.v1;

/// <summary>
/// Контрейлер агрегатор корзины
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class BasketController : ControllerBase
{
    private readonly IBasketApiClient _basketApiClient;
    private readonly ICatalogApiClient _catalogApiClient;

    public BasketController(IBasketApiClient basketApiClient, ICatalogApiClient catalogApiClient)
    {
        _basketApiClient = basketApiClient ?? throw new ArgumentNullException(nameof(basketApiClient));
        _catalogApiClient = catalogApiClient ?? throw new ArgumentNullException(nameof(catalogApiClient));
    }

    /// <summary>обновить кол-во</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="data">корзина</param>
    /// <returns>Каталог</returns>
    [HttpPut]
    [Route("items")]
    [ProducesResponseType(typeof(CustomerBasketResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateQuantitiesAsync([FromBody] UpdateBasketItemsRequest data, CancellationToken cancellationToken)
    {
        if (!data.Updates.Any())
        {
            return BadRequest(new ErrorViewModel("No updates sent"));
        }

        var currentBasket = await _basketApiClient.GetAsync(cancellationToken);

        if (currentBasket == null)
        {
            return NotFound(new ErrorViewModel($"Basket with id {data.BasketId} not found."));
        }
        foreach (var update in data.Updates)
        {
            var basketItem = currentBasket.Items.SingleOrDefault(bitem => bitem.Id == update.BasketItemId);
            if (basketItem == null)
            {
                return BadRequest(new ErrorViewModel($"Basket item with id {update.BasketItemId} not found"));
            }
            basketItem.Quantity = update.NewQty;
        }
        await _basketApiClient.UpdateAsync(currentBasket.Items, cancellationToken);
        return Ok(currentBasket);

    }

    /// <summary>обновить кол-во</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="data">корзина</param>
    /// <returns>Каталог</returns>
    [HttpPost]
    [Route("items")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> AddItemAsync([FromBody] AddBasketItemRequest data, CancellationToken cancellationToken)
    {
        if (data == null || data.Quantity == 0)
        {
            return BadRequest(new ErrorViewModel("Invalid payload"));
        }

        var item = await _catalogApiClient.GetAsync(data.CatalogItemId, cancellationToken);

        var currentBasket = await _basketApiClient.GetAsync(cancellationToken) ?? new CustomerBasketResponse() { BuyerId = data.BasketId };
        var product = currentBasket.Items.SingleOrDefault(i => i.ProductId == item.Id);
        if (product != null)
        {
            // Step 4: Update quantity for product
            product.Quantity += data.Quantity;
        }
        else
        {
            // Step 4: Merge current status with new product
            currentBasket.Items.Add(new BasketItemDto()
            {
                UnitPrice = item.Price,
                PictureUrl = item.PictureUri,
                ProductId = item.Id,
                ProductName = item.Name,
                Quantity = data.Quantity,
                Id = Guid.NewGuid().ToString()
            });
        }

        await _basketApiClient.UpdateAsync(currentBasket.Items, cancellationToken);
        return Ok();

    }

}
