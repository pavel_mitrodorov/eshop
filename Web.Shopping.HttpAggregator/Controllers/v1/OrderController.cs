﻿namespace Web.Shopping.HttpAggregator.Controllers.v1;

/// <summary>
/// Контрейлер агрегатор заказов
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class OrderController : ControllerBase
{
    private readonly IOrderApiClient _orderApiClient;
    private readonly IBasketApiClient _basketApiClient;

    public OrderController(IOrderApiClient orderApiClient, IBasketApiClient basketApiClient)
    {
        _orderApiClient = orderApiClient ?? throw new ArgumentNullException(nameof(orderApiClient));
        _basketApiClient = basketApiClient ?? throw new ArgumentNullException(nameof(basketApiClient));
    }

    /// <summary>черновик</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <returns>Каталог</returns>
    [HttpGet]
    [Route("draft")]
    [ProducesResponseType(typeof(OrderViewResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetDraftAsync(CancellationToken cancellationToken)
    {
        var basket = await _basketApiClient.GetAsync(cancellationToken);
        if (basket == null)
        {
            return NotFound(new ErrorViewModel("No basket found"));
        }
        var order = await _orderApiClient.CreateDraftFromBasketDataAsync(
            new CreateOrderDraftRequest()
            {
                BuyerId = Convert.ToInt32(basket.BuyerId),
                Items = basket.Items

            }, cancellationToken);
        return Ok(order);
    }
}
