﻿namespace Web.Shopping.HttpAggregator.Model
{
    public class UpdateBasketItemDto
    {
        [Required(ErrorMessage = "Не указано BasketItemId")]
        public string BasketItemId { get; set; }

        [Required(ErrorMessage = "Не указано NewQty")]
        [Range(0, double.MaxValue, ErrorMessage = "Недопустимое кол-во")]
        public decimal NewQty { get; set; }
    }
}
