﻿namespace Web.Shopping.HttpAggregator.Model;

public class UpdateBasketItemsRequest
{
    [Required(ErrorMessage = "Не указано BasketId")]
    public string BasketId { get; set; }

    [Required(ErrorMessage = "Не указано Updates")]
    public ICollection<UpdateBasketItemDto> Updates { get; set; }
}
