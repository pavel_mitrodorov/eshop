﻿namespace Web.Shopping.HttpAggregator.Model;

public class AddBasketItemRequest
{

    [Required(ErrorMessage = "Не указано CatalogItemId")]
    public int CatalogItemId { get; set; }


    [Required(ErrorMessage = "Не указано BasketId")]
    public string BasketId { get; set; }


    [Required(ErrorMessage = "Не указано Quantity")]
    [Range(0, double.MaxValue, ErrorMessage = "Недопустимое кол-во")]
    public decimal Quantity { get; set; }
}
