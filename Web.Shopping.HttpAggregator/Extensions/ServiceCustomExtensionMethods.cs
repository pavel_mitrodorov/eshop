﻿using Web.Shopping.HttpAggregator.Infrastructure.Filters;

namespace Web.Shopping.HttpAggregator.Extensions;

public static class ServiceCustomExtensionMethods
{
    public static IServiceCollection AddCustomMvc(this IServiceCollection services)
    {
        services
            .AddControllers(options => {
                options.SuppressAsyncSuffixInActionNames = false;
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));
            })
            .ConfigureApiBehaviorOptions(op =>
                op.SuppressModelStateInvalidFilter = true
            )
            .AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);

        return services;
    }

    public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, ApiClients apiClients)
    {
        services
            .AddHealthChecks()
            .AddUrlGroup(new Uri(apiClients.Catalog.HcHostName), name: "catalogapi-check", tags: new[] { "catalogapi" })
            .AddUrlGroup(new Uri(apiClients.Orders.HcHostName), name: "orderingapi-check", tags: new[] { "orderingapi" })
            .AddUrlGroup(new Uri(apiClients.Basket.HcHostName), name: "basketapi-check", tags: new[] { "basketapi" });

        return services;
    }

    public static IServiceCollection AddCustomApiClient(this IServiceCollection services, ApiClients apiClients)
    {
        services
            .AddOrderApiClient(conf =>
            {
                conf.BaseUrl = new Uri(apiClients.Orders.HostName);
                if (apiClients?.Orders.TimeOutInSec > 0)
                {
                    conf.HttpClientTimeOut = TimeSpan.FromSeconds(apiClients.Orders.TimeOutInSec); 
                }
            })
            .AddBasketApiClient(conf =>
            {
                conf.BaseUrl = new Uri(apiClients.Basket.HostName);
                if (apiClients?.Basket.TimeOutInSec > 0)
                {
                    conf.HttpClientTimeOut = TimeSpan.FromSeconds(apiClients.Basket.TimeOutInSec);
                }
            })
            .AddCatalogApiClient(conf =>
            {
                conf.BaseUrl = new Uri(apiClients.Catalog.HostName);
                if (apiClients?.Catalog.TimeOutInSec > 0)
                {
                    conf.HttpClientTimeOut = TimeSpan.FromSeconds(apiClients.Catalog.TimeOutInSec);
                }
            });

        return services;
    }

}
