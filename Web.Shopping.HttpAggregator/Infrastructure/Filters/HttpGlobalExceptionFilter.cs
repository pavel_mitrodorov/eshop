﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Text.Json;

namespace Web.Shopping.HttpAggregator.Infrastructure.Filters;

public class HttpGlobalExceptionFilter : IExceptionFilter
{
    private readonly ILogger<HttpGlobalExceptionFilter> _logger;
    private readonly IWebHostEnvironment _env;
    public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger, IWebHostEnvironment env)
    {
        _logger = logger;
        _env = env;
    }

    private  string GetErrorMessage(Exception exception)
    {
        return _env.IsDevelopment() ? $"message: {exception.Message}\n StackTrace: {exception.StackTrace}" 
                                    : $"message: {exception.Message}";
    }

    public void OnException(ExceptionContext context)
    {
        try
        {
            switch (context.Exception)
            {

                case ApiException e:


                    if (e.HasContent && (e.StatusCode == HttpStatusCode.BadRequest || e.StatusCode == HttpStatusCode.NotFound))
                    {

                        try
                        {
                            ErrorViewModel jsonApiException = JsonSerializer.Deserialize<ErrorViewModel>(e.Content);
                            switch(e.StatusCode)
                            {
                                case HttpStatusCode.NotFound:
                                    context.Result = new NotFoundObjectResult(jsonApiException);
                                    context.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
                                    break;
                                case HttpStatusCode.BadRequest:
                                    context.Result = new BadRequestObjectResult(jsonApiException);
                                    context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                                    break;
                            }
                        }
                        catch(Exception ex)
                        {
                            context.Result = new BadRequestObjectResult(new ErrorViewModel(GetErrorMessage(ex)));
                            context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                        }
                        
                    }
                    else
                    {
                        context.Result = new BadRequestObjectResult(new ErrorViewModel(GetErrorMessage(e)));
                        context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    }
                    break;
                case ArgumentNullException e:
                    string argumentNullExceptionMessage = $"Error in param: {e.ParamName} \n {GetErrorMessage(e)}";
                    var jsonArgumentNullException = new ErrorViewModel(argumentNullExceptionMessage, e.GetType().Name);
                    context.Result = new BadRequestObjectResult(jsonArgumentNullException);
                    context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    break;
                case TimeoutException e:
                    var jsonTimeoutException = new ErrorViewModel(GetErrorMessage(e), e.GetType().Name);
                    context.Result = new BadRequestObjectResult(jsonTimeoutException);
                    context.HttpContext.Response.StatusCode = StatusCodes.Status408RequestTimeout;
                    break;
                case Exception e:
                    string exceptionMessage = _env.IsDevelopment() ? GetErrorMessage(e) : "Error, something went wrong!";
                    context.Result = new ObjectResult(new ErrorViewModel(exceptionMessage, e.GetType().Name));
                    context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    break;
            }
            context.ExceptionHandled = true;
        }
        finally
        {
            _logger.LogError(new EventId(context.Exception.HResult), context.Exception, context.Exception.Message);
        }

    }
}
