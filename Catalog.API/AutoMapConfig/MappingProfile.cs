﻿namespace Catalog.API.AutoMapConfig;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        
        CreateMap<CreateCatalogItemDto, CatalogItem>().ReverseMap();
        CreateMap<UpdateCatalogItemDto, CatalogItem>().ReverseMap();
        CreateMap<CatalogItemDto, CatalogItem>().ReverseMap()
            .ForMember(dect => dect.Attachments,
                opt => opt.MapFrom(src => src.Attachments));
        CreateMap<AttachmentDTO, Attachment>().ReverseMap();

    }
}
