﻿using Catalog.API.AutoMapConfig;
using Catalog.API.Infrastructure.Filters;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Catalog.API.Extensions;

public static class ServiceCustomExtensionMethods
{
    

    public static IServiceCollection AddCustomServices(this IServiceCollection services)
    {
        services
            .AddScoped<ICatalogService, CatalogService>()
            .AddScoped<IAttachmentService, AttachmentService>();
        return services;
    }

    public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddMassTransit(x =>
            {
                x.AddConsumer<OrderStatusChangedToPaidIntegrationEventConsumer>();
                x.AddConsumer<OrderStatusChangedToAwaitingValidationIntegrationEventConsumer>();
                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["RabbitMQ:host"], configurator =>
                    {
                        configurator.Username(configuration["RabbitMQ:user"]);
                        configurator.Password(configuration["RabbitMQ:password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                    cfg.UseMessageRetry(r => r.Immediate(3));
                });
            });
        return services;
    }
    public static IServiceCollection AddCustomMvc(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddControllers(options => {
                options.SuppressAsyncSuffixInActionNames = false;
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));
            })
            .ConfigureApiBehaviorOptions(op =>
                op.SuppressModelStateInvalidFilter = true
            )
            .AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);

        return services;
    }

    public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
    {
        var hcBuilder = services.AddHealthChecks();

        hcBuilder
            .AddSqlServer(
                configuration["ConnectionString"],
                name: "CatalogDB-check",
                tags: new [] { "catalogdb" });
        //.AddRabbitMQ() -- HealthCheck на eventbus уже включен по умолчанию в MassTransit
        return services;
    }

    public static IServiceCollection AddAutoMappper(this IServiceCollection services)
    {
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        services.AddSingleton(mapperConfig.CreateMapper());
        return services;
    }

    public static IServiceCollection AddCustomDbContext<TContext>(
        this IServiceCollection services
        , IConfiguration configuration, 
        Action<TContext, ServiceProvider> initSeed = null
    ) where TContext : DbContext
    {
        services.AddDbContext<TContext>(options => {
            options.UseLazyLoadingProxies();
            options.UseSqlServer(configuration["ConnectionString"],
                                 sqlServerOptionsAction: sqlOptions =>
                                 {
                                     sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                                 }

            );
        });

        var sp = services.BuildServiceProvider();
        var context = sp.GetService<TContext>();
        var logger = sp.GetRequiredService<ILogger<TContext>>();
        if (context != null)
        {
            try
            {
                logger.LogInformation("Migrating database associated with context {DbContextName}", typeof(TContext).Name);
                context.Database.Migrate();
                logger.LogInformation("Migrated database associated with context {DbContextName}", typeof(TContext).Name);
                
                initSeed?.Invoke(context, sp);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred while migrating the database used on context {DbContextName}", typeof(TContext).Name);
            }

        }
        return services;
    }
}
