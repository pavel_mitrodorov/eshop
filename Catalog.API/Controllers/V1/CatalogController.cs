﻿namespace Catalog.API.Controllers.V1;

/// <summary>
/// Контрейлер в котором содержатся методы для получения каталога
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class CatalogController: ControllerBase
{
    private readonly IAppCache _cache;
    private readonly ICatalogService _catalogService;

    public CatalogController(ICatalogService catalogService, IAppCache cache)
    {
        _catalogService = catalogService ?? throw new ArgumentNullException(nameof(catalogService));
        _cache = cache ?? throw new ArgumentNullException(nameof(cache));
    }

    /// <summary>получение всего каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="pageSize">размер страницы</param>
    /// <param name="pageIndex">индекс страницы</param>
    /// <returns>Каталог</returns>
    [HttpGet]
    [Route("items")]
    [ProducesResponseType(typeof(PaginatedItemsViewModel<CatalogItemDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAsync(CancellationToken cancellationToken, [FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0)
    {        
        var model = await _catalogService.GetAsync(pageSize, pageIndex, cancellationToken);
        return Ok(model);
    }

    /// <summary>получение каталога по названию</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="name">наименование товара</param>
    /// <param name="pageSize">размер страницы</param>
    /// <param name="pageIndex">название</param>
    /// <returns>Каталог</returns>
    [HttpGet]
    [Route("items/withname/{name:minlength(1)}")]
    [ProducesResponseType(typeof(PaginatedItemsViewModel<CatalogItemDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetWithNameAsync(
        [FromRoute] string name, 
        CancellationToken cancellationToken, 
        [FromQuery] int pageSize = 10, 
        [FromQuery] int pageIndex = 0)
    {
        var model = await _catalogService.GetWithNameAsync(name, pageSize, pageIndex, cancellationToken);
        return Ok(model);
    }

    /// <summary>получение каталога по типу и бренду</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="catalogTypeId">тип</param>
    /// <param name="catalogBrandId">бренд</param>
    /// <param name="pageSize">размер страницы</param>
    /// <param name="pageIndex">название</param>
    /// <returns>Каталог</returns>
    [HttpGet]
    [Route("items/type/{catalogTypeId:int:min(0)}/brand/{catalogBrandId:int?}")]
    [ProducesResponseType(typeof(PaginatedItemsViewModel<CatalogItemDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetWithBrandAndTypeAsync(
        [FromRoute] int catalogTypeId, 
        [FromRoute] int? catalogBrandId,
        CancellationToken cancellationToken,
        [FromQuery] int pageSize = 10, 
        [FromQuery] int pageIndex = 0)
    {
        var model = await _catalogService.GetWithBrandAndTypeAsync(catalogTypeId, catalogBrandId, pageSize, pageIndex, cancellationToken);
        return Ok(model);
    }

    /// <summary>получение каталога по всем типам и бренду</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="catalogBrandId">бренд</param>
    /// <param name="pageSize">размер страницы</param>
    /// <param name="pageIndex">название</param>
    /// <returns>Каталог</returns>
    [HttpGet]
    [Route("items/type/all/brand/{catalogBrandId:int?}")]
    [ProducesResponseType(typeof(PaginatedItemsViewModel<CatalogItemDto>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetWithBrandAsync(
        [FromRoute] int? catalogBrandId, 
        CancellationToken cancellationToken, 
        [FromQuery] int pageSize = 10, 
        [FromQuery] int pageIndex = 0)
    {
        var model = await _catalogService.GetWithBrandAsync(catalogBrandId, pageSize, pageIndex, cancellationToken);
        return Ok(model);
    }

    /// <summary>получение одного элемента каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="id">id элемента</param>
    /// <returns>Элемент каталога</returns>
    [HttpGet]
    [Route("items/{id:int:min(0)}")]
    [ProducesResponseType(typeof(CatalogItemDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetAsync([FromRoute] int id, CancellationToken cancellationToken)
    {
        var item = await _catalogService.GetAsync(id, cancellationToken);
        if (item != null)
        {
            return Ok(item);
        }
        return NotFound(new ErrorViewModel($"Товар с id = {id} не найден!"));
    }


    /// <summary>создание элемента каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="catalogItemDto">элемент каталога</param>
    /// <returns>Элемент каталога</returns>
    [Route("items")]
    [HttpPost]
    [ProducesResponseType(typeof(CatalogItem), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CreateAsync([FromBody] CreateCatalogItemDto catalogItemDto, CancellationToken cancellationToken)
    {
        var item = await _catalogService.CreateAsync(catalogItemDto, cancellationToken);
        // ReSharper disable once Mvc.ActionNotResolved
        return CreatedAtAction(nameof(GetAsync), new { id = item.Id}, item);
    }

    /// <summary>обновление элемента каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="catalogItemId">id элемента каталога</param>
    /// <param name="catalogItemDto">элемент каталога</param>
    /// <returns>Элемент каталога</returns>
    [Route("items/{catalogItemId:int:min(0)}")]
    [HttpPut]
    [ProducesResponseType(typeof(CatalogItem), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateAsync([FromRoute] int catalogItemId, [FromBody] UpdateCatalogItemDto catalogItemDto, CancellationToken cancellationToken)
    {
        var item = await _catalogService.UpdateAsync(catalogItemId, catalogItemDto, cancellationToken);
        if (item == null)
        {
            return NotFound(new ErrorViewModel($"Товар с id = {catalogItemId} не найден!"));
        }
        // ReSharper disable once Mvc.ActionNotResolved
        return CreatedAtAction(nameof(GetAsync), new { id = item.Id }, item);
    }

    /// <summary>удаление элемента каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="id">id элемента каталога</param>
    /// <returns>успех да\нет?</returns>
    [Route("items/{id:int:min(0)}")]
    [HttpDelete]
    [ProducesResponseType(typeof(SuccessViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> DeleteAsync([FromRoute] int id, CancellationToken cancellationToken)
    {
        await _catalogService.DeleteAsync(id, cancellationToken);
        return Ok(new SuccessViewModel());
    }

    /// <summary>получение брендов каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <returns>Список брендов</returns>
    [HttpGet]
    [Route("catalogbrands")]
    [ProducesResponseType(typeof(List<CatalogBrand>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetBrandsAsync(CancellationToken cancellationToken)
    {
        var model = await _cache.GetOrAdd(
            CacheKey.CatalogBrands.ToString(),
            () => _catalogService.GetBrandsAsync(cancellationToken),
            new MemoryCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = new TimeSpan(8, 0, 0)
            });

        return Ok(model);
    }

    /// <summary>получение типов каталога</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <returns>Список типов</returns>
    [HttpGet]
    [Route("catalogtypes")]
    [ProducesResponseType(typeof(List<CatalogType>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetTypesAsync(CancellationToken cancellationToken)
    {
        var model = await _cache.GetOrAdd(
            CacheKey.CatalogTypes.ToString(),
            () => _catalogService.GetTypesAsync(cancellationToken),
            new MemoryCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = new TimeSpan(8, 0, 0)
            });
        
        return Ok(model);
    }
    
    /// <summary>загрузка и привязка фото к товару</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="id">id каталога</param>
    /// /// <param name="uploadedFile">uploadedFile</param>
    /// <returns>FileContentResult</returns>
    [HttpPost]
    [Route("{id:int}/load-and-link")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(AttachmentDTO), StatusCodes.Status201Created)]
    public async Task<IActionResult> LoadAndLinkAttachmentAsync(
        IFormFile uploadedFile, 
        [FromRoute] int id,
        CancellationToken cancellationToken)
    {
        var res = await _catalogService.LoadAndLinkAttachmentAsync(id, uploadedFile, cancellationToken);

        if (res == null)
        {
            return BadRequest(new ErrorViewModel($"не удалось загрузить вложение"));
        }
        
        // ReSharper disable once Mvc.ActionNotResolved
        return CreatedAtAction(nameof(AttachmentController.GetAsync), new { id = res.Id}, res);
    }
    
    /// <summary>фото продукта</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="id">id каталога</param>
    /// <returns>FileContentResult</returns>
    [HttpPost]
    [Route("{id:int}/attachments")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(List<AttachmentDTO>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAttachmentsAsync(
        [FromRoute] int id,
        CancellationToken cancellationToken)
    {
        var result = await _catalogService.GetAttachmentsAsync(id, cancellationToken);
        return Ok(result);
    }
}
