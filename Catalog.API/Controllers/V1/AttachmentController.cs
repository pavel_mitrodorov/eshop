﻿namespace Catalog.API.Controllers.V1;

/// <summary>
/// Контрейлер в котором содержатся методы для получения фото элементов каталога
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class AttachmentController : ControllerBase
{
    private readonly IAttachmentService _attachmentService;

    public AttachmentController(IAttachmentService attachmentService)
    {
        _attachmentService = attachmentService ?? throw new ArgumentNullException(nameof(attachmentService));
    }

    /// <summary>получение фото</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="id">id файла</param>
    /// <returns>Byte array</returns>
    [HttpGet]
    [Route("{id:guid}")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(FileContentResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAsync([FromRoute] Guid id, CancellationToken cancellationToken)
    {
        var file = await _attachmentService.GetAsync(id, cancellationToken);
        if (file.Item1 == null || file.Item2 == null)
        {
            return NotFound(new ErrorViewModel("фото не найдено или отсутствует"));
        }
        return File(file.Item1, file.Item2);
    }

    /// <summary>загрузка фото</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="uploadedFile"></param>
    /// <returns>FileContentResult</returns>
    [HttpPost]
    [Route("load")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(AttachmentDTO), StatusCodes.Status201Created)]
    public async Task<IActionResult> LoadAsync(IFormFile uploadedFile, CancellationToken cancellationToken)
    {
        var res = await _attachmentService.LoadAsync(uploadedFile, cancellationToken);

        if (res == null)
        {
            return BadRequest(new ErrorViewModel($"не удалось загрузить вложение"));
        }
        // ReSharper disable once Mvc.ActionNotResolved
        return CreatedAtAction(nameof(GetAsync), new { id = res.Id}, res);
    }

    /// <summary>привязка фото</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="attachmentId">id файла</param>
    /// <param name="body">body</param>
    /// <returns>File</returns>
    [HttpPut]
    [Route("{attachmentId:guid}/link")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(SuccessViewModel), StatusCodes.Status200OK)]
    public async Task<IActionResult> LinkToCatalogItemAsync(
        [FromRoute] Guid attachmentId,
        [FromBody] AttachmentLinkCatalogItemDto body,
        CancellationToken cancellationToken)
    {
        var res = await _attachmentService.LinkToCatalogItemAsync(body.CatalogItemId, attachmentId, cancellationToken);

        if (res == false)
        {
            return BadRequest(new ErrorViewModel($"не удалось привязать вложение к объекту"));
        }
        return Ok(new SuccessViewModel(true));
    }
    
    /// <summary>удалить фото</summary>
    /// <param name="cancellationToken">cancellationtoken</param>
    /// <param name="attachmentId">id файла</param>
    /// <returns>File</returns>
    [HttpDelete]
    [Route("{attachmentId:guid}")]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(SuccessViewModel), StatusCodes.Status200OK)]
    public async Task<IActionResult> DeleteAsync(
        [FromRoute] Guid attachmentId,
        CancellationToken cancellationToken)
    {
        var res = await _attachmentService.DeleteAsync(attachmentId, cancellationToken);

        if (res == false)
        {
            return BadRequest(new ErrorViewModel($"не удалось удалить вложение"));
        }
        return Ok(new SuccessViewModel(true));
    }
}
