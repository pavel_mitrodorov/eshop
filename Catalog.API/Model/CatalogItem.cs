﻿namespace Catalog.API.Model;

public class CatalogItem
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public decimal Price { get; set; }

    public virtual List<Attachment> Attachments { get; set; } = new();
    public int CatalogTypeId { get; set; }

    public virtual CatalogType CatalogType { get; set; }

    public int CatalogBrandId { get; set; }

    public virtual CatalogBrand CatalogBrand { get; set; }

    /// <summary>
    /// Количество на складе
    /// </summary>
    public int AvailableStock { get; set; }

    /// <summary>
    /// Доступный запас, при котором мы должны перезаказать
    /// </summary>
    public int RestockThreshold { get; set; }

    /// <summary>
    /// Максимальное количество единиц, которое может быть на складе
    /// </summary>
    public int MaxStockThreshold { get; set; }

    /// <summary>
    /// Истинно, если товар находится в повторном заказе
    /// </summary>
    public bool OnReorder { get; set; }
    
    /// <summary>
    /// Уменьшает количество на складе 
    /// </summary>
    /// <param name="quantityDesired">кол-во</param>
    /// <returns>int: Кол-во которое фактически списалось. </returns>
    /// 
    public int RemoveStock(int quantityDesired)
    {
        if (AvailableStock == 0)
        {
            throw new CatalogDomainException($"Данный товар {Name} уже распродан");
        }

        if (quantityDesired <= 0)
        {
            throw new CatalogDomainException($"Кол-во для списания должно быть > 0");
        }

        int removed = Math.Min(quantityDesired, this.AvailableStock);

        this.AvailableStock -= removed;

        return removed;
    }

    /// <summary>
    /// Увеличивает количество на складе
    /// <param name="quantity">кол-во</param>
    /// <returns>int: Кол-во которое фактически пришло.</returns>
    /// </summary>
    public int AddStock(int quantity)
    {
        int original = this.AvailableStock;

        if (quantity <= 0)
        {
            throw new CatalogDomainException($"Кол-во должно быть > 0");
        }

        // Количество, которое клиент пытается добавить на склад, превышает то, что физически может разместиться на складе
        if ((this.AvailableStock + quantity) > this.MaxStockThreshold)
        {
            // Этот метод добавляет новые единицы только до максимального порога запаса. 
            this.AvailableStock += (this.MaxStockThreshold - this.AvailableStock);
        }
        else
        {
            this.AvailableStock += quantity;
        }

        this.OnReorder = false;

        return this.AvailableStock - original;
    }

}
