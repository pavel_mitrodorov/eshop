﻿namespace Catalog.API.Model;

public class Attachment
{
    public Guid Id { get; set; }
    public string tag { get; set; }
    public string name { get; set; }
    public string path { get; set; }
    public virtual List<CatalogItem> CatalogItem { get; set; } = new(); 
    
}