﻿namespace Catalog.API.IntegrationServices.Consumers
{
    public class OrderStatusChangedToAwaitingValidationIntegrationEventConsumer
        : IConsumer<OrderStatusChangedToAwaitingValidationIntegrationEvent>
    {
        private readonly CatalogContext _catalogContext;
        private readonly IPublishEndpoint _publishEndpoint;
        public OrderStatusChangedToAwaitingValidationIntegrationEventConsumer(
            IPublishEndpoint publishEndpoint,
            CatalogContext catalogContext)
        {
            _catalogContext = catalogContext ?? throw new ArgumentNullException(nameof(catalogContext));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        }

        public async Task Consume(ConsumeContext<OrderStatusChangedToAwaitingValidationIntegrationEvent> context)
        {
            try
            {
                var confirmedOrderStockItems = new List<ConfirmedOrderStockItem>();
                var keys = context.Message.OrderStockItems
                    .Select(o => o.ProductId)
                    .Distinct();
                var catalogItems = await _catalogContext.CatalogItems
                    .Where(c => keys.Contains(c.Id))
                    .ToListAsync(context.CancellationToken);
                
                foreach(var orderStockItem in context.Message.OrderStockItems)
                {
                    var catalogItem = catalogItems?.Find(f => f.Id == orderStockItem.ProductId);
                    bool hasStock = catalogItem?.AvailableStock >= orderStockItem.Units;
                    if (catalogItem != null)
                    {
                        var confOrderStockItem = new ConfirmedOrderStockItem()
                        {
                            ProductId = catalogItem.Id,
                            HasStock = hasStock
                        };
                        confirmedOrderStockItems.Add(confOrderStockItem);                        
                    }

                }

                if(confirmedOrderStockItems.Any(c => !c.HasStock))
                {
                    await _publishEndpoint.Publish<OrderStockRejectedIntegrationEvent>(new
                    {
                        OrderId = context.Message.OrderId,
                        OrderStockItems = confirmedOrderStockItems
                    }, context.CancellationToken);
                }
                else
                {
                    await _publishEndpoint.Publish<OrderStockConfirmedIntegrationEvent>(new
                    {
                        OrderId = context.Message.OrderId
                    }, context.CancellationToken);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



        }
    }
}
