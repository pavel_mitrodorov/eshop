﻿namespace Catalog.API.IntegrationServices.Consumers
{
    public class OrderStatusChangedToPaidIntegrationEventConsumer
        : IConsumer<OrderStatusChangedToPaidIntegrationEvent>
    {

        
        private readonly CatalogContext _catalogContext; 
        public OrderStatusChangedToPaidIntegrationEventConsumer(CatalogContext catalogContext)
        {
            _catalogContext = catalogContext;
        }

        public async Task Consume(ConsumeContext<OrderStatusChangedToPaidIntegrationEvent> context)
        {
            var keys = context.Message.OrderStockItems
                .Select(k => k.ProductId)
                .Distinct();
            var catalogItems =await _catalogContext.CatalogItems
                .Where(c => keys.Contains(c.Id))
                .ToListAsync(context.CancellationToken);

            foreach( var orderStockItem in context.Message.OrderStockItems)
            {
                var catalogItem =  catalogItems.FirstOrDefault(c => c.Id == orderStockItem.ProductId);
                catalogItem?.RemoveStock(orderStockItem.Units);
            }
            await _catalogContext.SaveChangesAsync(context.CancellationToken);
        }
    }
}
