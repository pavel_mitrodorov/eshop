﻿namespace Catalog.API.DTO;

public class AttachmentDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}