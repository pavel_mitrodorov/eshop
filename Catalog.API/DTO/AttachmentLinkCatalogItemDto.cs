﻿namespace Catalog.API.DTO;

public class AttachmentLinkCatalogItemDto
{
    [Required(ErrorMessage = "Не указан CatalogItemId")]
    public int CatalogItemId { get; set; }
}