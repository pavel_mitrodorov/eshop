﻿namespace Catalog.API.DTO
{
    public class UpdateCatalogItemDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CatalogBrandId { get; set; }
        public int CatalogTypeId { get; set; }
        public int MaxStockThreshold { get; set; }
        public int RestockThreshold { get; set; }
        public int AvailableStock { get; set; }

    }
}
