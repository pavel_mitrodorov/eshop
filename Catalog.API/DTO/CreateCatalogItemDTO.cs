﻿namespace Catalog.API.DTO
{
    public class CreateCatalogItemDto
    {
        [Required(ErrorMessage = "Не указано Name")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 150 символов")]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required(ErrorMessage = "Не указано Price")]
        [Range(0, double.MaxValue, ErrorMessage = "Недопустимая цена")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Не указано CatalogBrandId")]
        public int CatalogBrandId { get; set; }

        [Required(ErrorMessage = "Не указано CatalogTypeId")]
        public int CatalogTypeId { get; set; }

    }
}
