﻿namespace Catalog.API.DTO;

public class CatalogItemDto
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public decimal Price { get; set; }
    
    public List<AttachmentDTO> Attachments { get; set; }
    
    public CatalogType CatalogType { get; set; }

    public CatalogBrand CatalogBrand { get; set; }

    public int AvailableStock { get; set; }

    public int RestockThreshold { get; set; }

    public int MaxStockThreshold { get; set; }

    public bool OnReorder { get; set; }

}
