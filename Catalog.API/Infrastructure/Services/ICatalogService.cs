﻿namespace Catalog.API.Infrastructure.Services;

public interface ICatalogService
{
    Task<PaginatedItemsViewModel<CatalogItemDto>> GetAsync(int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default);
    Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithNameAsync(string name, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default);
    Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithBrandAndTypeAsync(int catalogTypeId, int? catalogBrandId, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default);
    Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithBrandAsync(int? catalogBrandId, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default);
    Task<CatalogItemDto> GetAsync(int id, CancellationToken cancellationToken = default);
    Task<CatalogItem> CreateAsync(CreateCatalogItemDto catalogItemDto, CancellationToken cancellationToken = default);
    Task<CatalogItem> UpdateAsync(int catalogItemId, UpdateCatalogItemDto catalogItemDto, CancellationToken cancellationToken = default);
    Task DeleteAsync(int catalogItemId, CancellationToken cancellationToken = default);
    Task<IEnumerable<CatalogBrand>> GetBrandsAsync(CancellationToken cancellationToken = default);
    Task<IEnumerable<CatalogType>> GetTypesAsync(CancellationToken cancellationToken = default);
    Task<List<AttachmentDTO>> GetAttachmentsAsync(int catalogItemId, CancellationToken cancellationToken = default);
    Task<AttachmentDTO> LoadAndLinkAttachmentAsync(int catalogItemId, IFormFile uploadedFile, CancellationToken cancellationToken = default);
}
