﻿namespace Catalog.API.Infrastructure.Services;

public class CatalogService
    : ICatalogService
{
    private readonly CatalogContext _catalogContext;
    private readonly IMapper _mapper;
    private readonly IConfiguration _config;
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly IAttachmentService _attachmentService;
    public CatalogService(
        CatalogContext catalogContext, 
        IMapper mapper, 
        IConfiguration config, 
        IPublishEndpoint publishEndpoint,
        IAttachmentService attachmentService)
    {
        _catalogContext = catalogContext ?? throw new ArgumentNullException(nameof(catalogContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _config = config ?? throw new ArgumentNullException(nameof(config));
        _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        _attachmentService = attachmentService ?? throw new ArgumentNullException(nameof(attachmentService));
    }

    public async Task<CatalogItem> CreateAsync(CreateCatalogItemDto catalogItemDto, CancellationToken cancellationToken = default)
    {
        var item = _mapper.Map<CatalogItem>(catalogItemDto);
        _catalogContext.CatalogItems.Add(item);
        await _catalogContext.SaveChangesAsync(cancellationToken);
        return item;
    }

    public async Task DeleteAsync(int catalogItemId, CancellationToken cancellationToken = default)
    {
        var item = await _catalogContext.CatalogItems.SingleOrDefaultAsync(i => i.Id == catalogItemId, cancellationToken);
        if (item == null)
        {
            throw new CatalogDomainException($"Товар с id = {catalogItemId} не найден!");
        }
        _catalogContext.CatalogItems.Remove(item);
        await _catalogContext.SaveChangesAsync(cancellationToken);
    }

    public async Task<IEnumerable<CatalogBrand>> GetBrandsAsync(CancellationToken cancellationToken = default)
    {
        return await _catalogContext.CatalogBrands.ToListAsync(cancellationToken);
    }

    public async Task<IEnumerable<CatalogType>> GetTypesAsync(CancellationToken cancellationToken = default)
    {
        return await _catalogContext.CatalogTypes.ToListAsync(cancellationToken);
    }

    public async Task<CatalogItemDto> GetAsync(int id, CancellationToken cancellationToken = default)
    {
        var catalogItem = await _catalogContext
            .CatalogItems
            .SingleOrDefaultAsync(c => c.Id == id, cancellationToken);
            
        return _mapper.Map<CatalogItemDto>(catalogItem);
    }

    public async Task<PaginatedItemsViewModel<CatalogItemDto>> GetAsync(int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default)
    {
        var totalItems = await _catalogContext.CatalogItems
            .LongCountAsync(cancellationToken);

        var itemsOnPage = await _catalogContext
            .CatalogItems
            .OrderBy(c => c.Name)
            .ThenBy(c => c.Price)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync(cancellationToken);

        var itemsOnPageDto = _mapper.Map<List<CatalogItemDto>>(itemsOnPage);
        return new PaginatedItemsViewModel<CatalogItemDto>(pageIndex, pageSize, totalItems, itemsOnPageDto);
    }

    public async Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithBrandAndTypeAsync(int catalogTypeId, int? catalogBrandId, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogItem> root = _catalogContext.CatalogItems;
        root = root.Where(c => c.CatalogTypeId == catalogTypeId);

        if (catalogBrandId.HasValue)
        {
            root = root.Where(c => c.CatalogBrandId == catalogBrandId);
        }


        var totalItems = await root
            .LongCountAsync(cancellationToken);

        var itemsOnPage = await root
            .OrderBy(c => c.Name)
            .ThenBy(c => c.Price)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync(cancellationToken);
        
        var itemsOnPageDto = _mapper.Map<List<CatalogItemDto>>(itemsOnPage);
        
        return  new PaginatedItemsViewModel<CatalogItemDto>(pageIndex, pageSize, totalItems, itemsOnPageDto);
    }

    public async Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithBrandAsync(int? catalogBrandId, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogItem> root = _catalogContext.CatalogItems;

        if (catalogBrandId.HasValue)
        {
            root = root.Where(c => c.CatalogBrandId == catalogBrandId);
        }

        var totalItems = await root
            .LongCountAsync(cancellationToken);

        var itemsOnPage = await root
            .OrderBy(c => c.Name)
            .ThenBy(c => c.Price)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync(cancellationToken);

        var itemsOnPageDto = _mapper.Map<List<CatalogItemDto>>(itemsOnPage);
        
        return new PaginatedItemsViewModel<CatalogItemDto>(pageIndex, pageSize, totalItems, itemsOnPageDto);
    }

    public async Task<PaginatedItemsViewModel<CatalogItemDto>> GetWithNameAsync(string name, int pageSize = 10, int pageIndex = 0, CancellationToken cancellationToken = default)
    {
        var totalItems = await _catalogContext.CatalogItems
            .Where(c => c.Name.StartsWith(name))
            .LongCountAsync(cancellationToken);

        var itemsOnPage = await _catalogContext
            .CatalogItems
            .Where(c => c.Name.StartsWith(name))
            .OrderBy(c => c.Name)
            .ThenBy(c => c.Price)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync(cancellationToken);

        var itemsOnPageDto = _mapper.Map<List<CatalogItemDto>>(itemsOnPage);
        
        return new PaginatedItemsViewModel<CatalogItemDto>(pageIndex, pageSize, totalItems, itemsOnPageDto);
    }

    public async Task<CatalogItem> UpdateAsync(int catalogItemId, UpdateCatalogItemDto catalogItemDto, CancellationToken cancellationToken = default)
    {
        var item = await _catalogContext.CatalogItems.SingleOrDefaultAsync(i => i.Id == catalogItemId, cancellationToken);
        if (item == null)
        {
            return null;
        }

        decimal oldPrice = item.Price;
        if (oldPrice != catalogItemDto.Price)
        {
            await _publishEndpoint.Publish<ProductPriceChangedIntegrationEvent>(new
            {
                ProductId = item.Id,
                NewPrice = catalogItemDto.Price,
                OldPrice = oldPrice
            }, cancellationToken);
        }

        _mapper.Map(catalogItemDto, item);
        _catalogContext.CatalogItems.Update(item);
        await _catalogContext.SaveChangesAsync(cancellationToken);
        return item;
    }
    
    public async Task<List<AttachmentDTO>> GetAttachmentsAsync(int catalogItemId, CancellationToken cancellationToken = default)
    {
        var catalogItem = await _catalogContext
            .CatalogItems
            .SingleOrDefaultAsync(f => f.Id == catalogItemId, cancellationToken);

        var result = catalogItem
            ?.Attachments
            .ToList();
        
        return _mapper.Map<List<AttachmentDTO>>(result);
    }
    
    public async Task<AttachmentDTO> LoadAndLinkAttachmentAsync(
        int catalogItemId, 
        IFormFile uploadedFile,
        CancellationToken cancellationToken = default)
    {
        var file = await _attachmentService.LoadAsync(uploadedFile, cancellationToken);
        if (!await _attachmentService.LinkToCatalogItemAsync(catalogItemId, file.Id, cancellationToken))
        {
            return default;
        }
        return file;
    }
}
