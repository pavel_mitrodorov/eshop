﻿namespace Catalog.API.Infrastructure.Services;

public class AttachmentService
    : IAttachmentService
{

    private readonly CatalogContext _catalogContext;
    private readonly IWebHostEnvironment _env;
    private readonly IConfiguration _conf;
    private readonly ILogger<AttachmentService> _logger;
    private readonly IMapper _mapper;

    public AttachmentService(
        CatalogContext catalogContext, 
        IWebHostEnvironment env, 
        IConfiguration conf,
        ILogger<AttachmentService> logger,
        IMapper mapper
        )
    {
        _catalogContext = catalogContext ?? throw new ArgumentNullException(nameof(catalogContext));
        _env = env ?? throw new ArgumentNullException(nameof(env));
        _conf = conf ?? throw new ArgumentNullException(nameof(conf));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    private string GetImageMimeTypeFromImageFileExtension(string extension)
    {
        string mimetype;

        switch (extension)
        {
            case ".png":
                mimetype = "image/png";
                break;
            case ".gif":
                mimetype = "image/gif";
                break;
            case ".jpg":
            case ".jpeg":
                mimetype = "image/jpeg";
                break;
            case ".bmp":
                mimetype = "image/bmp";
                break;
            case ".tiff":
                mimetype = "image/tiff";
                break;
            case ".wmf":
                mimetype = "image/wmf";
                break;
            case ".jp2":
                mimetype = "image/jp2";
                break;
            case ".svg":
                mimetype = "image/svg+xml";
                break;
            default:
                mimetype = "application/octet-stream";
                break;
        }

        return mimetype;
    }
    private string GetFilePath(string addFileNameToPath = "")
    {
        string webroot = _env.WebRootPath;
        return Path.Combine(webroot, "images", addFileNameToPath);
    }
    
    public async Task<(byte[], string)> GetAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var file = await _catalogContext
            .Attachment
            .SingleOrDefaultAsync(f => f.Id == id, cancellationToken);
        
        if (file == null || string.IsNullOrEmpty(file.path))
        {
            return (null, null);
        }
        var buffer = await File.ReadAllBytesAsync(file.path, cancellationToken);
        string imageFileExtension = Path.GetExtension(file.path);
        string mimetype = GetImageMimeTypeFromImageFileExtension(imageFileExtension);
        return (buffer, mimetype);

    }

    public async Task<AttachmentDTO> LoadAsync(IFormFile uploadedFile, CancellationToken cancellationToken)
    {
        Guid id = Guid.NewGuid();
        
        string fileName = $"{id}_{uploadedFile.FileName}";
        
        var newFile = new Attachment()
        {
            name = fileName,
            path = GetFilePath(fileName),
            Id = id
        };
        _catalogContext.Attachment.Add(newFile);
        
        using (var fileStream = new FileStream(newFile.path, FileMode.Create))
        {
            await uploadedFile.CopyToAsync(fileStream, cancellationToken);
        }

        await _catalogContext.SaveChangesAsync(cancellationToken);

        return _mapper.Map<AttachmentDTO>(newFile);
    }

    public async Task<bool> LinkToCatalogItemAsync(int catalogItemId, Guid fileId, CancellationToken cancellationToken = default)
    {
        var file = _catalogContext.Attachment.FirstOrDefault(f => f.Id == fileId); 
        var catalog = _catalogContext.CatalogItems.FirstOrDefault(x => x.Id == catalogItemId);
        catalog?.Attachments.Add(file);
        return await _catalogContext.SaveChangesAsync(cancellationToken) > 0;
    }
    
    public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var file = await _catalogContext
            .Attachment
            .SingleOrDefaultAsync(f => f.Id == id, cancellationToken);
        
        if (file == null || string.IsNullOrEmpty(file.path))
        {
            return false;
        }
        
        _catalogContext.Attachment.Remove(file);
        
        try
        {
            File.Delete(file.path);
            return await _catalogContext.SaveChangesAsync(cancellationToken) > 0;
        }
        catch (Exception e)
        {
            _logger
                .LogWarning($"Failed to attachment file id {id} {e.Message}");
            
            return false;
        }
    }
    
}
