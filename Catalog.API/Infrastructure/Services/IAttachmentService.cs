﻿namespace Catalog.API.Infrastructure.Services;

public interface IAttachmentService
{
    Task<(byte[], string)> GetAsync(Guid id, CancellationToken cancellationToken = default);
    Task<AttachmentDTO> LoadAsync(IFormFile uploadedFile, CancellationToken cancellationToken = default);
    Task<bool> LinkToCatalogItemAsync(int catalogItemId, Guid fileId, CancellationToken cancellationToken = default);
    Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
}
