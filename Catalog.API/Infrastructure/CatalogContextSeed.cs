﻿using Microsoft.Data.SqlClient;
using Polly;
using Polly.Retry;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Catalog.API.Infrastructure;

public class CatalogContextSeed
{
    
    public async Task SeedAsync(CatalogContext context, ILogger<CatalogContextSeed> logger)
    {
        var policy = CreatePolicy(logger, nameof(CatalogContextSeed));
        await policy.ExecuteAsync(async () =>
        {
            if (!context.CatalogBrands.Any())
            {
                var brands = new List<CatalogBrand>()
                {
                    new CatalogBrand {Brand = "M2"},
                    new CatalogBrand {Brand = "Петруха"},
                    new CatalogBrand {Brand = "Фрост"}
                };
                logger.LogInformation("Start seed CatalogBrands data");
                context.CatalogBrands.AddRange(brands);
                logger.LogInformation("End seed CatalogBrands data");
            }

            if (!context.CatalogTypes.Any())
            {
                var types = new List<CatalogType>()
                {
                    new CatalogType {Type = "Production"},
                    new CatalogType {Type = "Other Production"}
                };
                logger.LogInformation("Start seed CatalogTypes data");
                context.CatalogTypes.AddRange(types);
                logger.LogInformation("End seed CatalogTypes data");
            }

            if (!context.CatalogItems.Any())
            {
                var items = new List<CatalogItem>()
                {
                    new CatalogItem { Name = "Творог", CatalogBrandId = 1, CatalogTypeId = 1, AvailableStock = 100, MaxStockThreshold = 100, Price = 10, RestockThreshold =20},
                    new CatalogItem { Name = "Сметана", CatalogBrandId = 1, CatalogTypeId = 1, AvailableStock = 100, MaxStockThreshold = 100, Price = 10, RestockThreshold = 20 }
                    
                };
                logger.LogInformation("Start seed CatalogItems data");
                context.CatalogItems.AddRange(items);
                logger.LogInformation("End seed CatalogItems data");
            }

            await context.SaveChangesAsync();
        });
    }
    
    private AsyncRetryPolicy CreatePolicy(ILogger<CatalogContextSeed> logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().
            WaitAndRetryAsync(
                retryCount: retries,
                sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                onRetry: (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retry, retries);
                }
            );
    }
    
}