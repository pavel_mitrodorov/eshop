﻿namespace Catalog.API.Infrastructure.EntityConfigurations;

public class AttachmentEntityTypeConfiguration
    : IEntityTypeConfiguration<Attachment>
{
    public void Configure(EntityTypeBuilder<Attachment> builder)
    {
        builder.ToTable("Attachment");
        builder.HasKey(ci => ci.Id);
        
        builder.HasIndex(ci => ci.path)
            .IsUnique();
        
    }
}
