﻿namespace Catalog.API.Infrastructure;
public enum CacheKey
{
    CatalogBrands,
    CatalogTypes
}
